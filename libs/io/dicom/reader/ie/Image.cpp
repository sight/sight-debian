/************************************************************************
 *
 * Copyright (C) 2009-2023 IRCAD France
 * Copyright (C) 2012-2020 IHU Strasbourg
 *
 * This file is part of Sight.
 *
 * Sight is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Sight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Sight. If not, see <https://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

#include "io/dicom/reader/ie/Image.hpp"

#include "io/dicom/exception/Failed.hpp"
#include "io/dicom/helper/DicomDataReader.hxx"
#include "io/dicom/helper/DicomDataTools.hpp"

#include <data/dicom/Image.hpp>
#include <data/DicomSeries.hpp>
#include <data/helper/MedicalImage.hpp>
#include <data/Image.hpp>

#include <geometry/data/VectorFunctions.hpp>

#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>

#include <gdcmImageApplyLookupTable.h>
#include <gdcmImageHelper.h>
#include <gdcmImageReader.h>
#include <gdcmIPPSorter.h>
#include <gdcmPixelFormat.h>
#include <gdcmRescaler.h>
#include <gdcmUIDGenerator.h>

#include <glm/common.hpp>
#include <glm/ext/scalar_constants.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace sight::io::dicom::reader::ie
{

//------------------------------------------------------------------------------

Image::Image(
    const data::DicomSeries::csptr& dicomSeries,
    const SPTR(gdcm::Reader)& reader,
    const io::dicom::container::DicomInstance::sptr& instance,
    const data::Image::sptr& image,
    const core::log::Logger::sptr& logger,
    ProgressCallback progress,
    CancelRequestedCallback cancel
) :
    io::dicom::reader::ie::InformationEntity<data::Image>(dicomSeries, reader, instance, image,
                                                          logger, progress, cancel)
{
}

//------------------------------------------------------------------------------

Image::~Image()
= default;

//------------------------------------------------------------------------------

double getInstanceZPosition(const core::memory::BufferObject::sptr& bufferObj)
{
    gdcm::ImageReader reader;
    const core::memory::BufferManager::StreamInfo streamInfo = bufferObj->getStreamInfo();
    SPTR(std::istream) is = streamInfo.stream;
    reader.SetStream(*is);

    if(!reader.Read())
    {
        return 0;
    }

    // Retrieve dataset
    const gdcm::DataSet& dataset = reader.GetFile().GetDataSet();

    // Check tags availability
    if(!dataset.FindDataElement(gdcm::Tag(0x0020, 0x0032)) || !dataset.FindDataElement(gdcm::Tag(0x0020, 0x0037)))
    {
        const std::string msg = "Unable to compute the spacing between slices of the series.";
        throw io::dicom::exception::Failed(msg);
    }

    // Retrieve image position
    const gdcm::Image& gdcmImage = reader.GetImage();
    const double* gdcmOrigin     = gdcmImage.GetOrigin();
    const fwVec3d imagePosition  = {gdcmOrigin[0], gdcmOrigin[1], gdcmOrigin[2]};

    // Retrieve image orientation
    const double* directionCosines  = gdcmImage.GetDirectionCosines();
    const fwVec3d imageOrientationU = {
        std::round(directionCosines[0]),
        std::round(directionCosines[1]),
        std::round(directionCosines[2])
    };
    const fwVec3d imageOrientationV = {
        std::round(directionCosines[3]),
        std::round(directionCosines[4]),
        std::round(directionCosines[5])
    };

    //Compute Z direction (cross product)
    const fwVec3d zVector = geometry::data::cross(imageOrientationU, imageOrientationV);

    //Compute dot product to get the index
    const double index = geometry::data::dot(imagePosition, zVector);

    return index;
}

//------------------------------------------------------------------------------

void Image::readImagePlaneModule()
{
    // Retrieve GDCM image
    SPTR(gdcm::ImageReader) imageReader = std::static_pointer_cast<gdcm::ImageReader>(m_reader);
    const gdcm::Image& gdcmImage = imageReader->GetImage();

    // Image Position (Patient) - Type 1
    const double* gdcmOrigin   = gdcmImage.GetOrigin();
    data::Image::Origin origin = {0., 0., 0.
    };
    if(gdcmOrigin != nullptr)
    {
        std::copy(gdcmOrigin, gdcmOrigin + 3, origin.begin());
    }

    m_object->setOrigin(origin);

    // Pixel Spacing - Type 1
    // Image dimension
    const unsigned int dimension = gdcmImage.GetNumberOfDimensions();

    // Image's spacing
    const double* gdcmSpacing    = gdcmImage.GetSpacing();
    data::Image::Spacing spacing = {1., 1., 1.
    };
    if(gdcmSpacing != nullptr)
    {
        std::copy(gdcmSpacing, gdcmSpacing + dimension, spacing.begin());
    }

    // Computing Z image spacing from two adjacent slices is preferable than reading Slice Thickness tag
    // See http://gdcm.sourceforge.net/wiki/index.php/Imager_Pixel_Spacing, Spacing along Z (third dimension)
    const data::DicomSeries::DicomContainerType dicomContainer = m_dicomSeries->getDicomContainer();
    if(dicomContainer.size() > 1)
    {
        auto firstItem       = dicomContainer.begin();
        const auto& lastItem = dicomContainer.rbegin();

        // Compute the spacing between slices of the 2 first slices.
        const double firstIndex  = getInstanceZPosition(firstItem->second);
        const double secondIndex = getInstanceZPosition((++firstItem)->second);
        const double lastIndex   = getInstanceZPosition(lastItem->second);
        spacing[2] = std::abs(secondIndex - firstIndex);

        // Check that the same spacing is used for all the instances
        const double epsilon       = 1e-2;
        const double totalZSpacing = std::abs(lastIndex - firstIndex);
        const double errorGap      = std::abs(spacing[2] * static_cast<double>(dicomContainer.size() - 1))
                                     - totalZSpacing;
        if(errorGap > epsilon)
        {
            std::stringstream ss;
            ss << "Computed spacing between slices may not be correct. (Error gap : " << errorGap << ")";
            m_logger->warning(ss.str());
        }
    }
    else
    {
        // Retrieve dataset
        const gdcm::DataSet& dataset = imageReader->GetFile().GetDataSet();

        // Check tags availability
        if(dataset.FindDataElement(gdcm::Tag(0x0018, 0x0050)))
        {
            const std::string& sliceThickness =
                io::dicom::helper::DicomDataReader::getTagValue<0x0018, 0x0050>(dataset);
            spacing[2] = std::stod(sliceThickness);
        }
    }

    m_object->setSpacing(spacing);
}

//------------------------------------------------------------------------------

void Image::readVOILUTModule()
{
    // Retrieve dataset
    const gdcm::DataSet& dataset = m_reader->GetFile().GetDataSet();

    //Image's window center (double)
    std::string windowCenter = io::dicom::helper::DicomDataReader::getTagValue<0x0028, 0x1050>(dataset);
    std::vector<std::string> splitedWindowCenters;
    if(!windowCenter.empty())
    {
        boost::split(splitedWindowCenters, windowCenter, boost::is_any_of("\\"));
        std::vector<double> windowCenters;
        std::ranges::transform(
            splitedWindowCenters,
            std::back_inserter(windowCenters),
            [](const auto& e){return std::stod(e);});

        m_object->setWindowCenter(windowCenters);
    }

    //Image's window width (double)
    std::string windowWidth = io::dicom::helper::DicomDataReader::getTagValue<0x0028, 0x1051>(dataset);
    std::vector<std::string> splitedWindowWidths;
    if(!windowWidth.empty())
    {
        boost::split(splitedWindowWidths, windowWidth, boost::is_any_of("\\"));
        std::vector<double> windowWidths;
        std::ranges::transform(
            splitedWindowWidths,
            std::back_inserter(windowWidths),
            [](const auto& e){return std::stod(e);});

        m_object->setWindowWidth(windowWidths);
    }
}

//------------------------------------------------------------------------------

std::vector<double> getRescaleInterceptSlopeValue(gdcm::ImageReader* imageReader)
{
    // Retrieve dataset
    const gdcm::DataSet& dataset = imageReader->GetFile().GetDataSet();

    // Retrieve rescale values
    std::vector<double> rescale = gdcm::ImageHelper::GetRescaleInterceptSlopeValue(imageReader->GetFile());

    // Correct Rescale Intercept and Rescale Slope as GDCM may fail to retrieve them.
    if(dataset.FindDataElement(gdcm::Tag(0x0028, 0x1052)) && dataset.FindDataElement(gdcm::Tag(0x0028, 0x1053)))
    {
        rescale[0] = io::dicom::helper::DicomDataReader::getTagValue<0x0028, 0x1052, double>(dataset);
        rescale[1] = io::dicom::helper::DicomDataReader::getTagValue<0x0028, 0x1053, double>(dataset);
    }

    return rescale;
}

//------------------------------------------------------------------------------

void Image::readImagePixelModule()
{
    // Retrieve GDCM image
    SPTR(gdcm::ImageReader) imageReader = std::static_pointer_cast<gdcm::ImageReader>(m_reader);
    const gdcm::Image& gdcmImage = imageReader->GetImage();

    // Retrieve dataset
    const gdcm::DataSet& dataset = m_reader->GetFile().GetDataSet();

    // Retrieve image information before processing the rescaling
    gdcm::PixelFormat pixelFormat = gdcm::ImageHelper::GetPixelFormatValue(imageReader->GetFile());

    // Retrieve rescale intercept/slope values
    std::vector<double> rescale = getRescaleInterceptSlopeValue(imageReader.get());
    double rescaleIntercept     = rescale[0];
    double rescaleSlope         = rescale[1];

    const std::uint16_t samplesPerPixel     = pixelFormat.GetSamplesPerPixel();
    const std::uint16_t bitsAllocated       = pixelFormat.GetBitsAllocated();
    const std::uint16_t bitsStored          = pixelFormat.GetBitsStored();
    const std::uint16_t highBit             = pixelFormat.GetHighBit();
    const std::uint16_t pixelRepresentation = pixelFormat.GetPixelRepresentation();

    // Compute final image type
    data::dicom::Image imageHelper(
        samplesPerPixel, bitsAllocated, bitsStored, highBit, pixelRepresentation, rescaleSlope, rescaleIntercept);
    core::Type imageType                = imageHelper.findImageTypeFromMinMaxValues();
    gdcm::PixelFormat targetPixelFormat = io::dicom::helper::DicomDataTools::getPixelType(imageType);

    if(targetPixelFormat == gdcm::PixelFormat::UNKNOWN)
    {
        throw io::dicom::exception::Failed("Unsupported image pixel format.");
    }

    // Compute number of components
    const std::string photometricInterpretation =
        io::dicom::helper::DicomDataReader::getTagValue<0x0028, 0x0004>(dataset);
    const std::string pixelPresentation =
        io::dicom::helper::DicomDataReader::getTagValue<0x0008, 0x9205>(dataset);

    // Retrieve image dimensions
    std::vector<unsigned int> dimensions = gdcm::ImageHelper::GetDimensionsValue(imageReader->GetFile());

    // Compute real image size (we assume every instance has the same number of
    // slices (1 for CT and MR, may be more for enhanced CT and MR)
    const std::uint64_t frameBufferSize = gdcmImage.GetBufferLength();
    const std::uint64_t depth           = frameBufferSize
                                          / (std::uint64_t(dimensions[0]) * dimensions[1] * (bitsAllocated / 8));
    dimensions[2] = static_cast<unsigned int>(m_dicomSeries->getDicomContainer().size() * depth);

    const std::uint64_t imageBufferSize =
        std::uint64_t(dimensions[0]) * dimensions[1] * dimensions[2] * (bitsAllocated / 8);
    const std::uint64_t newImageBufferSize =
        std::uint64_t(dimensions[0]) * dimensions[1] * dimensions[2] * (targetPixelFormat.GetBitsAllocated() / 8);

    // Let's read the image buffer
    bool performRescale = (photometricInterpretation != "PALETTE COLOR" && pixelPresentation != "COLOR");
    char* imageBuffer   = this->readImageBuffer(
        dimensions,
        imageType,
        bitsAllocated,
        targetPixelFormat.GetBitsAllocated(),
        performRescale
    );

    // Correct image buffer according to the image orientation
    if(!(m_cancelRequestedCallback && m_cancelRequestedCallback()) && m_enableBufferRotation)
    {
        imageBuffer = this->correctImageOrientation(
            imageBuffer,
            dimensions,
            (performRescale) ? targetPixelFormat.GetBitsAllocated() : bitsAllocated
        );
    }

    // Apply lookup table if required
    if(!(m_cancelRequestedCallback && m_cancelRequestedCallback())
       && (photometricInterpretation == "PALETTE COLOR" || pixelPresentation == "COLOR"))
    {
        try
        {
            // Create new buffer
            char* coloredBuffer = nullptr;
            coloredBuffer = new char [newImageBufferSize * 3];

            // Apply lookup
            gdcmImage.GetLUT().Decode(coloredBuffer, newImageBufferSize * 3, imageBuffer, imageBufferSize);

            // Swap buffers
            delete[] imageBuffer;
            imageBuffer = coloredBuffer;
        }
        catch(...)
        {
            throw io::dicom::exception::Failed("There is not enough memory available to open this image.");
        }
    }

    // TODO_FB: This should probably be finer-tuned, but we would need to add new pixel formats before
    sight::data::Image::PixelFormat format {sight::data::Image::PixelFormat::UNDEFINED};
    if(photometricInterpretation == "MONOCHROME2")
    {
        format = data::Image::PixelFormat::GRAY_SCALE;
    }
    else if(photometricInterpretation == "RGB" || photometricInterpretation == "YBR"
            || photometricInterpretation == "PALETTE COLOR" || pixelPresentation == "COLOR")
    {
        format = data::Image::PixelFormat::RGB;
    }
    else if(photometricInterpretation == "ARGB" || photometricInterpretation == "CMYK")
    {
        format = data::Image::PixelFormat::RGBA;
    }
    else
    {
        const std::string msg = "The photometric interpretation \"" + photometricInterpretation
                                + "\" is not supported.";
        throw io::dicom::exception::Failed(msg);
    }

    // Last, set image buffer
    m_object->setBuffer(
        imageBuffer,
        true,
        imageType,
        {dimensions[0], dimensions[1], dimensions[2]},
        format,
        core::memory::BufferNewPolicy::New()
    );

    if(sight::data::helper::MedicalImage::checkImageValidity(m_object))
    {
        sight::data::helper::MedicalImage::checkImageSliceIndex(m_object);
    }
}

//------------------------------------------------------------------------------

char* Image::readImageBuffer(
    const std::vector<unsigned int>& dimensions,
    const core::Type imageType,
    const std::uint16_t bitsAllocated,
    const std::uint16_t newBitsAllocated,
    const bool performRescale
)
{
    // Retrieve GDCM image
    SPTR(gdcm::ImageReader) imageReader = std::static_pointer_cast<gdcm::ImageReader>(m_reader);
    const gdcm::Image& gdcmFirstImage = imageReader->GetImage();

    // Path container
    data::DicomSeries::DicomContainerType dicomContainer = m_dicomSeries->getDicomContainer();

    // Raw buffer for all frames
    char* frameBuffer                      = nullptr;
    char* imageBuffer                      = nullptr;
    const std::uint64_t frameBufferSize    = gdcmFirstImage.GetBufferLength();
    const std::uint64_t newFrameBufferSize = frameBufferSize * (newBitsAllocated / bitsAllocated);
    const std::uint64_t imageBufferSize    = std::uint64_t(dimensions.at(0)) * dimensions.at(1) * dimensions.at(2)
                                             * ((performRescale ? newBitsAllocated : bitsAllocated) / 8);

    // Allocate raw buffer
    try
    {
        frameBuffer = new char [frameBufferSize];
        imageBuffer = new char [imageBufferSize];
    }
    catch(...)
    {
        throw io::dicom::exception::Failed("There is not enough memory available to open this image.");
    }

    // Read every frames
    unsigned int frameNumber = 0;
    for(const auto& item : dicomContainer)
    {
        // Read a frame
        gdcm::ImageReader frameReader;
        const core::memory::BufferObject::sptr bufferObj         = item.second;
        const core::memory::BufferManager::StreamInfo streamInfo = bufferObj->getStreamInfo();
        const std::string dicomPath                              = bufferObj->getStreamInfo().fsFile.string();
        SPTR(std::istream) is = streamInfo.stream;
        frameReader.SetStream(*is);

        if(frameReader.Read())
        {
            const gdcm::Image& gdcmImage = frameReader.GetImage();

            // Check frame buffer size
            if(frameBufferSize != gdcmImage.GetBufferLength())
            {
                throw io::dicom::exception::Failed("The frame buffer does not have the expected size : " + dicomPath);
            }

            // Get raw buffer and set it in the image buffer
            if(!gdcmImage.GetBuffer(frameBuffer))
            {
                throw io::dicom::exception::Failed("Failed to get a frame buffer");
            }
        }
        else
        {
            std::stringstream ss;
            ss << "Reading error on frame : " << frameNumber;
            throw io::dicom::exception::Failed(ss.str());
        }

        // Rescale
        if(performRescale)
        {
            // Retrieve rescale intercept/slope values
            std::vector<double> rescale = getRescaleInterceptSlopeValue(&frameReader);
            double rescaleIntercept     = rescale[0];
            double rescaleSlope         = rescale[1];

            // Retrieve image information before processing the rescaling
            gdcm::PixelFormat pixelFormat =
                gdcm::ImageHelper::GetPixelFormatValue(frameReader.GetFile());
            gdcm::PixelFormat::ScalarType scalarType = pixelFormat.GetScalarType();
            gdcm::PixelFormat targetPixelFormat      = io::dicom::helper::DicomDataTools::getPixelType(imageType);

            if(targetPixelFormat == gdcm::PixelFormat::UNKNOWN)
            {
                throw io::dicom::exception::Failed("Unsupported image pixel format.");
            }

            // Create rescaler
            gdcm::Rescaler rescaler;
            rescaler.SetIntercept(rescaleIntercept);
            rescaler.SetSlope(rescaleSlope);
            rescaler.SetPixelFormat(scalarType);
            rescaler.SetTargetPixelType(targetPixelFormat.GetScalarType());
            rescaler.SetUseTargetPixelType(true);

            // Rescale the image
            rescaler.Rescale(imageBuffer + (frameNumber * newFrameBufferSize), frameBuffer, frameBufferSize);
        }
        else
        {
            // Copy bytes
            memcpy(imageBuffer + frameNumber * frameBufferSize, frameBuffer, frameBufferSize);
        }

        // Reference SOP Instance UID in dicomInstance for SR reading
        const gdcm::DataSet& gdcmDatasetRoot = frameReader.GetFile().GetDataSet();
        const std::string sopInstanceUID     = io::dicom::helper::DicomDataReader::getTagValue<0x0008, 0x0018>(
            gdcmDatasetRoot
        );
        if(!sopInstanceUID.empty())
        {
            m_instance->getSOPInstanceUIDContainer().push_back(sopInstanceUID);
        }
        else
        {
            m_logger->warning("A frame with an undefined SOP instance UID has been detected.");
        }

        // Next frame
        ++frameNumber;

        auto progress =
            static_cast<unsigned int>(18 + (frameNumber * 100 / static_cast<double>(dicomContainer.size())) * 0.6);
        m_progressCallback(progress);

        if(m_cancelRequestedCallback && m_cancelRequestedCallback())
        {
            break;
        }
    }

    // Delete frame buffer
    delete[] frameBuffer;

    return imageBuffer;
}

//------------------------------------------------------------------------------

char* Image::correctImageOrientation(
    char* buffer,
    std::vector<unsigned int>& dimensions,
    std::uint16_t bitsAllocated
)
{
    char* result = buffer;

    // Retrieve GDCM image
    SPTR(gdcm::ImageReader) imageReader = std::static_pointer_cast<gdcm::ImageReader>(m_reader);
    const gdcm::Image& gdcmImage = imageReader->GetImage();

    // Retrieve image orientation
    const double* directionCosines = gdcmImage.GetDirectionCosines();

    // Compute U vector
    glm::dvec3 imageOrientationU = {
        std::round(directionCosines[0]),
        std::round(directionCosines[1]),
        std::round(directionCosines[2])
    };

    // Try to find the closest axe
    if((std::fabs(imageOrientationU[0]) + std::fabs(imageOrientationU[1]) + std::fabs(imageOrientationU[2])) > 1)
    {
        if(std::fabs(directionCosines[0]) < std::fabs(directionCosines[1])
           || std::fabs(directionCosines[0]) < std::fabs(directionCosines[2]))
        {
            imageOrientationU[0] = 0.;
        }

        if(std::fabs(directionCosines[1]) < std::fabs(directionCosines[0])
           || std::fabs(directionCosines[1]) < std::fabs(directionCosines[2]))
        {
            imageOrientationU[1] = 0.;
        }

        if(std::fabs(directionCosines[2]) < std::fabs(directionCosines[0])
           || std::fabs(directionCosines[2]) < std::fabs(directionCosines[1]))
        {
            imageOrientationU[2] = 0.;
        }

        m_logger->warning(
            "Unable to determine clearly the orientation of the image. "
            "The software may display the image in the wrong direction."
        );
    }

    // Compute V vector
    glm::dvec3 imageOrientationV = {
        std::round(directionCosines[3]),
        std::round(directionCosines[4]),
        std::round(directionCosines[5])
    };

    // Try to find the closest axe
    if((std::fabs(imageOrientationV[0]) + std::fabs(imageOrientationV[1]) + std::fabs(imageOrientationV[2])) > 1)
    {
        if(std::fabs(directionCosines[3]) < std::fabs(directionCosines[4])
           || std::fabs(directionCosines[3]) < std::fabs(directionCosines[5]))
        {
            imageOrientationV[0] = 0.;
        }

        if(std::fabs(directionCosines[4]) < std::fabs(directionCosines[3])
           || std::fabs(directionCosines[4]) < std::fabs(directionCosines[5]))
        {
            imageOrientationV[1] = 0.;
        }

        if(std::fabs(directionCosines[5]) < std::fabs(directionCosines[3])
           || std::fabs(directionCosines[5]) < std::fabs(directionCosines[4]))
        {
            imageOrientationV[2] = 0.;
        }

        m_logger->warning(
            "Unable to determine clearly the orientation of the image. "
            "The software may display the image in the wrong direction."
        );
    }

    // Compute W vector
    const glm::dvec3 imageOrientationW = glm::cross(imageOrientationU, imageOrientationV);

    // Create orientation matrix
    glm::dmat4 matrix;
    matrix[0][0] = imageOrientationU[0];
    matrix[0][1] = imageOrientationU[1];
    matrix[0][2] = imageOrientationU[2];
    matrix[0][3] = 0;
    matrix[1][0] = imageOrientationV[0];
    matrix[1][1] = imageOrientationV[1];
    matrix[1][2] = imageOrientationV[2];
    matrix[1][3] = 0;
    matrix[2][0] = imageOrientationW[0];
    matrix[2][1] = imageOrientationW[1];
    matrix[2][2] = imageOrientationW[2];
    matrix[2][3] = 0;
    matrix[3][0] = 0;
    matrix[3][1] = 0;
    matrix[3][2] = 0;
    matrix[3][3] = 1;

    // Compute inverse matrix in order to rotate the buffer
    const glm::dmat4 inverseMatrix = glm::inverse(matrix);
    const auto identityMatrix      = glm::identity<glm::dmat4>();

    // Check whether the image must be rotated or not
    if(inverseMatrix != identityMatrix)
    {
        // Compute new image size
        glm::dvec4 sizeVector {dimensions.at(0), dimensions.at(1), dimensions.at(2), 0.};
        glm::dvec4 newSizeVector = sizeVector * inverseMatrix;
        const auto newSizeX      = static_cast<std::uint16_t>(std::fabs(newSizeVector[0]));
        const auto newSizeY      = static_cast<std::uint16_t>(std::fabs(newSizeVector[1]));
        const auto newSizeZ      = static_cast<std::uint16_t>(std::fabs(newSizeVector[2]));
        newSizeVector.x = newSizeX;
        newSizeVector.y = newSizeY;
        newSizeVector.z = newSizeZ;

        // Compute old size from the new absolute size in order to retrieve pixel flips
        glm::dvec4 oldSizeVector = newSizeVector * matrix;

        // Create new buffer to store rotated image
        const std::uint64_t size = std::uint64_t(dimensions.at(0)) * dimensions.at(1) * dimensions.at(2)
                                   * (bitsAllocated / 8);
        char* newBuffer = new char [size];

        // Rotate image
        std::uint16_t x     = 0;
        std::uint16_t y     = 0;
        std::uint16_t z     = 0;
        std::uint16_t old_x = 0;
        std::uint16_t old_y = 0;
        std::uint16_t old_z = 0;
        for(z = 0 ; z < newSizeZ && !(m_cancelRequestedCallback && m_cancelRequestedCallback()) ; ++z)
        {
            for(y = 0 ; y < newSizeY ; ++y)
            {
                for(x = 0 ; x < newSizeX ; ++x)
                {
                    // Create new position
                    glm::dvec4 newPosition {x, y, z, 0.};

                    // Compute old position
                    glm::dvec4 oldPosition = newPosition * matrix;
                    old_x = (oldSizeVector[0] > 0) ? static_cast<std::uint16_t>(oldPosition[0])
                                                   : static_cast<std::uint16_t>((dimensions.at(0) - 1)
                                                                                + oldPosition[0]);
                    old_y = (oldSizeVector[1] > 0) ? static_cast<std::uint16_t>(oldPosition[1])
                                                   : static_cast<std::uint16_t>((dimensions.at(1) - 1)
                                                                                + oldPosition[1]);
                    old_z = (oldSizeVector[2] > 0) ? static_cast<std::uint16_t>(oldPosition[2])
                                                   : static_cast<std::uint16_t>((dimensions.at(2) - 1)
                                                                                + oldPosition[2]);

                    // Compute indices
                    unsigned int positionIndex = (x + (y * newSizeX) + z * (newSizeX * newSizeY))
                                                 * (bitsAllocated / 8);
                    unsigned int oldPositionIndex =
                        (old_x + (old_y * dimensions.at(0)) + old_z * (dimensions.at(0) * dimensions.at(1)))
                        * (bitsAllocated / 8);

                    // Copy bytes
                    memcpy(&newBuffer[positionIndex], &buffer[oldPositionIndex], (bitsAllocated / 8));
                }
            }

            auto progress = static_cast<unsigned int>(78 + (z * 100. / newSizeZ) * 0.2);
            m_progressCallback(progress);
        }

        result = newBuffer;
        delete[] buffer;

        dimensions = {newSizeX, newSizeY, newSizeZ};

        // Update image spacing
        const data::Image::Spacing spacing = m_object->getSpacing();
        glm::dvec4 spacingVector {spacing[0], spacing[1], spacing[2], 0.};
        glm::dvec4 newSpacingVector = spacingVector * inverseMatrix;
        data::Image::Spacing newSpacing {std::fabs(newSpacingVector[0]), std::fabs(newSpacingVector[1]), std::fabs(
                                             newSpacingVector[2]
        )
        };
        m_object->setSpacing(newSpacing);

        // Update image origin
        const data::Image::Origin origin = m_object->getOrigin();
        glm::dvec4 originVector {origin[0], origin[1], origin[2], 0.};
        glm::dvec4 newOriginVector = originVector * inverseMatrix;
        data::Image::Origin newOrigin;
        newOrigin[0] = newOriginVector[0];
        newOrigin[1] = newOriginVector[1];
        newOrigin[2] = newOriginVector[2];
        m_object->setOrigin(newOrigin);

        m_logger->warning(
            "Image buffer has been rotated in order to match patient orientation: "
            "image origin could be wrong."
        );
    }

    return result;
}

//------------------------------------------------------------------------------

} // namespace sight::io::dicom::reader::ie
