/************************************************************************
 *
 * Copyright (C) 2009-2022 IRCAD France
 * Copyright (C) 2012-2020 IHU Strasbourg
 *
 * This file is part of Sight.
 *
 * Sight is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Sight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Sight. If not, see <https://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

#pragma once

#include "core/config.hpp"
#include "core/runtime/Profile.hpp"

#include <filesystem>
#include <functional>
#include <map>
#include <vector>
namespace sight::core::runtime::detail::profile
{

class Activater;
class Starter;

/**
 * @brief   Implements a module set profile.
 */
class Profile : public core::runtime::Profile
{
public:

    typedef std::vector<std::string> ParamsContainer;
    typedef std::function<int ()> RunCallbackType;

    SIGHT_DECLARE_CLASS(Profile, BaseObject, std::make_shared<Profile>);

    /**
     * @brief   Constructor : does nothing.
     */
    Profile();

    ~Profile() override;

    /**
     * @brief       Adds a new activator.
     *
     * @param[in]   activater   a shared pointer to an activator
     */
    void add(SPTR(Activater) activater);

    /**
     * @brief       Adds a new starter.
     *
     * @param[in]   starter a shared pointer to a starter
     */
    void addStarter(const std::string& identifier);

    /**
     * @brief       Adds a new stopper.
     *
     * @param[in]   stopper a shared pointer to a stopper
     */
    void addStopper(const std::string& identifier, int priority);

    /**
     * @brief   Starts the profile.
     */
    void start() final;
    void stop() final;

    /**
     * @brief   Run the profile.
     */
    int run() final;
    void setRunCallback(RunCallbackType callback) final;

    static int defaultRun();

    /**
     * @brief   Return profile CheckSingleInstance.
     */
    bool getCheckSingleInstance() const
    {
        return m_checkSingleInstance;
    }

    /**
     * @brief       Set profile CheckSingleInstance.
     *
     * @param[in]   _checkSingleInstance    profile CheckSingleInstance
     */
    void setCheckSingleInstance(bool _checkSingleInstance)
    {
        m_checkSingleInstance = _checkSingleInstance;
    }

private:

    typedef std::vector<SPTR(Activater)> ActivaterContainer;
    typedef std::multimap<int, std::string> StarterContainer;
    typedef std::multimap<int, std::string> StopperContainer;

    ActivaterContainer m_activaters; ///< all managed activators
    StarterContainer m_starters;     ///< all managed starters
    StopperContainer m_stoppers;     ///< all managed stoppers

    RunCallbackType m_run;

    bool m_checkSingleInstance {false};
};

/**
 * @brief       Set current profile.
 *
 * @param       prof profile
 */
void setCurrentProfile(Profile::sptr prof);

/**
 * @brief       Get current profile.
 */
Profile::sptr getCurrentProfile();

} // namespace sight::core::runtime::detail::profile
