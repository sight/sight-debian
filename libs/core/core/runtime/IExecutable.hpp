/************************************************************************
 *
 * Copyright (C) 2009-2022 IRCAD France
 * Copyright (C) 2012-2020 IHU Strasbourg
 *
 * This file is part of Sight.
 *
 * Sight is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Sight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Sight. If not, see <https://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

#pragma once

#include "core/config.hpp"

#include <memory>
#include <string>

namespace sight::core::runtime
{

namespace utils
{

template<typename E>
class GenericExecutableFactory;

} // namespace utils

class Module;
class Runtime;

/**
 * @brief   Defines the base executable interface.
 *
 * An executable object is an instance created by an extension
 * point of a plugin.
 */
class CORE_CLASS_API IExecutable
{
public:

    template<typename E>
    friend class core::runtime::utils::GenericExecutableFactory;

    /**
     * @brief   Destructor : does nothing.
     */
    CORE_API virtual ~IExecutable();

    /**
     * @brief   Retrieves the module the executable originates from.
     *
     * @return  a pointer to the originating module.
     */
    [[nodiscard]] virtual std::shared_ptr<Module> getModule() const = 0;

protected:

    /**
     * @brief       Updates the module the executable originates from.
     *
     * @param[in]   module  a pointer to the module the executable originates from
     */
    virtual void setModule(std::shared_ptr<Module> module) = 0;
};

} // namespace sight::core::runtime
