/************************************************************************
 *
 * Copyright (C) 2022-2023 IRCAD France
 *
 * This file is part of Sight.
 *
 * Sight is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Sight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Sight. If not, see <https://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

#include "service/detail/ServiceConnection.hpp"

#include "core/com/Proxy.hpp"

#include "service/IService.hpp"

#include <core/com/Signal.hxx>

namespace sight::service::detail
{

//-----------------------------------------------------------------------------

void ServiceConnection::connect(const service::IService& _service)
{
    core::com::Proxy::sptr proxy = core::com::Proxy::get();

    for(const auto& proxyCfg : m_proxies)
    {
        for(const auto& signalCfg : proxyCfg.second.m_signals)
        {
            SIGHT_ASSERT("Invalid signal source", signalCfg.first == _service.getID());

            core::com::SignalBase::sptr sig = _service.signal(signalCfg.second);
            SIGHT_ASSERT("Signal '" + signalCfg.second + "' not found in source '" + signalCfg.first + "'.", sig);
            try
            {
                proxy->connect(proxyCfg.second.m_channel, sig);
            }
            catch(const std::exception& e)
            {
                SIGHT_ERROR(
                    "Signal '" + signalCfg.second + "' from '" + signalCfg.first
                    + "' can not be connected to the channel '" + proxyCfg.second.m_channel
                    + "': " + std::string(e.what())
                );
            }
        }

        for(const auto& slotCfg : proxyCfg.second.m_slots)
        {
            // The start slot is connected before the start stage in connectStartSlot(), skip it
            if(slotCfg.second == IService::slots::s_START)
            {
                continue;
            }

            SIGHT_ASSERT("Invalid slot destination", slotCfg.first == _service.getID());

            core::com::SlotBase::sptr slot = _service.slot(slotCfg.second);
            SIGHT_ASSERT("Slot '" + slotCfg.second + "' not found in source '" + slotCfg.first + "'.", slot);

            try
            {
                proxy->connect(proxyCfg.second.m_channel, slot);
            }
            catch(const std::exception& e)
            {
                SIGHT_ERROR(
                    "Slot '" + slotCfg.second + "' from '" + slotCfg.first
                    + "' can not be connected to the channel '" + proxyCfg.second.m_channel
                    + "': " + std::string(e.what())
                );
            }
        }
    }
}

//-----------------------------------------------------------------------------

void ServiceConnection::connectStartSlot(const service::IService& _service)
{
    core::com::Proxy::sptr proxy = core::com::Proxy::get();

    for(const auto& proxyCfg : m_proxies)
    {
        for(const auto& slotCfg : proxyCfg.second.m_slots)
        {
            if(slotCfg.second != IService::slots::s_START)
            {
                continue;
            }

            SIGHT_ASSERT("Invalid slot destination", slotCfg.first == _service.getID());

            core::com::SlotBase::sptr slot = _service.slot(slotCfg.second);
            SIGHT_ASSERT("Slot '" + slotCfg.second + "' not found in source '" + slotCfg.first + "'.", slot);

            try
            {
                proxy->connect(proxyCfg.second.m_channel, slot);
            }
            catch(const std::exception& e)
            {
                SIGHT_ERROR(
                    "Slot '" + slotCfg.second + "' from '" + slotCfg.first + "' can not be connected to the "
                                                                             "channel '" + proxyCfg.second.m_channel + "': " + std::string(
                        e.what()
                                                                             )
                );
            }
        }
    }
}

//-----------------------------------------------------------------------------

void ServiceConnection::disconnect(const service::IService& _service)
{
    core::com::Proxy::sptr proxy = core::com::Proxy::get();

    for(const auto& proxyCfg : m_proxies)
    {
        for(const auto& signalCfg : proxyCfg.second.m_signals)
        {
            SIGHT_ASSERT("Invalid signal source", signalCfg.first == _service.getID());

            core::com::SignalBase::sptr sig = _service.signal(signalCfg.second);

            try
            {
                proxy->disconnect(proxyCfg.second.m_channel, sig);
            }
            catch(const std::exception& e)
            {
                SIGHT_ERROR(
                    "Signal '" + signalCfg.second + "' from '" + signalCfg.first + "' can not be disconnected "
                                                                                   "from the channel '" + proxyCfg.second.m_channel + "': " + std::string(
                        e.what()
                                                                                   )
                );
            }
        }

        for(const auto& slotCfg : proxyCfg.second.m_slots)
        {
            SIGHT_ASSERT("Invalid slot destination", slotCfg.first == _service.getID());

            // The start slot is disconnected after the stop stage in disconnectStartSlot(), skip it
            if(slotCfg.second == IService::slots::s_START)
            {
                continue;
            }

            core::com::SlotBase::sptr slot = _service.slot(slotCfg.second);
            try
            {
                proxy->disconnect(proxyCfg.second.m_channel, slot);
            }
            catch(const std::exception& e)
            {
                SIGHT_ERROR(
                    "Slot '" + slotCfg.second + "' from '" + slotCfg.first + "' can not be disconnected from the "
                                                                             "channel '" + proxyCfg.second.m_channel + "': " + std::string(
                        e.what()
                                                                             )
                );
            }
        }
    }
}

//-----------------------------------------------------------------------------

void ServiceConnection::disconnectStartSlot(const service::IService& _service)
{
    core::com::Proxy::sptr proxy = core::com::Proxy::get();

    for(const auto& proxyCfg : m_proxies)
    {
        for(const auto& slotCfg : proxyCfg.second.m_slots)
        {
            SIGHT_ASSERT("Invalid slot destination", slotCfg.first == _service.getID());

            if(slotCfg.second != IService::slots::s_START)
            {
                continue;
            }

            core::com::SlotBase::sptr slot = _service.slot(slotCfg.second);
            try
            {
                proxy->disconnect(proxyCfg.second.m_channel, slot);
            }
            catch(const std::exception& e)
            {
                SIGHT_ERROR(
                    "Slot '" + slotCfg.second + "' from '" + slotCfg.first + "' can not be disconnected from the "
                                                                             "channel '" + proxyCfg.second.m_channel + "': " + std::string(
                        e.what()
                                                                             )
                );
            }
        }
    }
}

//-----------------------------------------------------------------------------

void ServiceConnection::add(const helper::ProxyConnections& proxy)
{
    m_proxies[proxy.m_channel] = proxy;
}

} // namespace sight::service::detail
