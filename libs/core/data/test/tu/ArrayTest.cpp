/************************************************************************
 *
 * Copyright (C) 2009-2022 IRCAD France
 * Copyright (C) 2012-2020 IHU Strasbourg
 *
 * This file is part of Sight.
 *
 * Sight is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Sight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Sight. If not, see <https://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

// cspell:ignore NOLINTNEXTLINE

#include "ArrayTest.hpp"

#include <data/Array.hpp>
#include <data/Exception.hpp>

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(sight::data::ut::ArrayTest);

namespace sight::data::ut
{

//-----------------------------------------------------------------------------

void ArrayTest::setUp()
{
    // Set up context before running a test.
}

//-----------------------------------------------------------------------------

void ArrayTest::tearDown()
{
    // Clean up after the test run.
}

//-----------------------------------------------------------------------------

void ArrayTest::allocation()
{
    data::Array::sptr array = data::Array::New();
    auto lock               = array->dump_lock();

    CPPUNIT_ASSERT(array->empty());
    CPPUNIT_ASSERT(array->getBuffer() == nullptr);
    CPPUNIT_ASSERT(array->getSize().empty());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(0), array->getSizeInBytes());

    data::Array::SizeType size = {10, 100};

    array->resize(size, core::Type::UINT32, true);
    CPPUNIT_ASSERT(array->getBuffer() != nullptr);
    CPPUNIT_ASSERT(!array->empty());

    CPPUNIT_ASSERT_EQUAL(size.size(), array->numDimensions());
    CPPUNIT_ASSERT_EQUAL(size[0], array->getSize()[0]);
    CPPUNIT_ASSERT_EQUAL(size[1], array->getSize()[1]);
    CPPUNIT_ASSERT_EQUAL(true, array->getIsBufferOwner());
    {
        data::Array::OffsetType stride = {4, 40};
        CPPUNIT_ASSERT(array->getStrides() == stride);
    }
    CPPUNIT_ASSERT_EQUAL(core::Type::UINT32, array->getType());
    CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(4), array->getType().size());

    array->clear();
    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(0), array->getSizeInBytes());
    CPPUNIT_ASSERT(array->empty());
    CPPUNIT_ASSERT(array->getBuffer() == nullptr);
    CPPUNIT_ASSERT_EQUAL(core::Type::NONE, array->getType());

    auto* buffer = new std::uint16_t[1000];

    for(std::uint16_t i = 0 ; i < 1000 ; i++)
    {
        buffer[i] = i;
    }

    array->setBuffer(buffer, false, size, core::Type::UINT16, core::memory::BufferNewPolicy::New());

    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(2), array->getElementSizeInBytes());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(2 * 10 * 100), array->getSizeInBytes());
    {
        data::Array::OffsetType stride = {2, 20};
        CPPUNIT_ASSERT(array->getStrides() == stride);
    }
    CPPUNIT_ASSERT_EQUAL(buffer[0], array->at<std::uint16_t>({0, 0}));
    CPPUNIT_ASSERT_EQUAL(buffer[10], array->at<std::uint16_t>({0, 1}));
    CPPUNIT_ASSERT_EQUAL(buffer[999], array->at<std::uint16_t>({9, 99}));
    CPPUNIT_ASSERT_EQUAL(buffer[326], array->at<std::uint16_t>({6, 32}));
    CPPUNIT_ASSERT_EQUAL(buffer[947], array->at<std::uint16_t>({7, 94}));
    CPPUNIT_ASSERT_EQUAL(buffer[238], array->at<std::uint16_t>({8, 23}));
    CPPUNIT_ASSERT_EQUAL(false, array->getIsBufferOwner());

    array->clear();

    delete[] buffer;
}

//-----------------------------------------------------------------------------

void ArrayTest::resize()
{
    data::Array::sptr array = data::Array::New();

    data::Array::SizeType size {10, 100};

    array->resize(size, core::Type::UINT32, true);
    auto lock = array->dump_lock();

    CPPUNIT_ASSERT(array->getBuffer() != nullptr);

    std::uint32_t count = 0;

    auto iter = array->begin<std::uint32_t>();

    for( ; iter != array->end<std::uint32_t>() ; ++iter)
    {
        *iter = count++;
    }

    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(4), array->getElementSizeInBytes());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(4 * 10 * 100), array->getSizeInBytes());
    {
        data::Array::OffsetType stride = {4, 40};
        CPPUNIT_ASSERT(array->getStrides() == stride);
    }
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(0), array->at<std::uint32_t>({0, 0}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(10), array->at<std::uint32_t>({0, 1}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(999), array->at<std::uint32_t>({9, 99}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(326), array->at<std::uint32_t>({6, 32}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(947), array->at<std::uint32_t>({7, 94}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(238), array->at<std::uint32_t>({8, 23}));

    data::Array::SizeType newSize = {100, 10};

    array->resize(newSize);
    CPPUNIT_ASSERT(newSize == array->getSize());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(4), array->getElementSizeInBytes());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(4 * 10 * 100), array->getSizeInBytes());
    {
        data::Array::OffsetType stride = {4, 400};
        CPPUNIT_ASSERT(array->getStrides() == stride);
    }
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(0), array->at<std::uint32_t>({0, 0}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(10), array->at<std::uint32_t>({10, 0}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(999), array->at<std::uint32_t>({99, 9}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(326), array->at<std::uint32_t>({26, 3}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(947), array->at<std::uint32_t>({47, 9}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(238), array->at<std::uint32_t>({38, 2}));

    newSize.clear();
    newSize = {25, 40};

    array->resize(newSize);
    CPPUNIT_ASSERT(newSize == array->getSize());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(4), array->getElementSizeInBytes());
    {
        data::Array::OffsetType stride = {4, 100};
        CPPUNIT_ASSERT(array->getStrides() == stride);
    }
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(0), array->at<std::uint32_t>({0, 0}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(10), array->at<std::uint32_t>({10, 0}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(999), array->at<std::uint32_t>({24, 39}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(326), array->at<std::uint32_t>({1, 13}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(947), array->at<std::uint32_t>({22, 37}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(238), array->at<std::uint32_t>({13, 9}));

    newSize.clear();

    newSize = {2, 10, 100};

    array->resize(newSize, core::Type::UINT16, false);
    CPPUNIT_ASSERT(newSize == array->getSize());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(2), array->getElementSizeInBytes());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(2 * 100 * 10 * 2), array->getSizeInBytes());
    {
        data::Array::OffsetType stride = {2, 4, 40};
        CPPUNIT_ASSERT(array->getStrides() == stride);
    }
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(0), array->at<std::uint32_t>({0, 0, 0}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(10), array->at<std::uint32_t>({0, 0, 1}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(999), array->at<std::uint32_t>({0, 9, 99}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(326), array->at<std::uint32_t>({0, 6, 32}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(947), array->at<std::uint32_t>({0, 7, 94}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(238), array->at<std::uint32_t>({0, 8, 23}));
}

//-----------------------------------------------------------------------------

void ArrayTest::reallocate()
{
    data::Array::sptr array = data::Array::New();

    data::Array::SizeType size = {10, 100};

    array->resize(size, core::Type::UINT32, true);
    auto lock = array->dump_lock();

    std::uint32_t count = 0;
    auto iter           = array->begin<std::uint32_t>();
    auto iterEnd        = array->end<std::uint32_t>();
    for( ; iter != iterEnd ; ++iter)
    {
        *iter = count++;
    }

    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(4 * 10 * 100), array->getSizeInBytes());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(0), array->at<std::uint32_t>({0, 0}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(10), array->at<std::uint32_t>({0, 1}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(999), array->at<std::uint32_t>({9, 99}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(326), array->at<std::uint32_t>({6, 32}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(947), array->at<std::uint32_t>({7, 94}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(238), array->at<std::uint32_t>({8, 23}));

    data::Array::SizeType newSize = {100, 100};

    array->resize(newSize, true);
    CPPUNIT_ASSERT(newSize == array->getSize());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(4 * 100 * 100), array->getSizeInBytes());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(0), array->at<std::uint32_t>({0, 0}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(10), array->at<std::uint32_t>({10, 0}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(999), array->at<std::uint32_t>({99, 9}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(326), array->at<std::uint32_t>({26, 3}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(947), array->at<std::uint32_t>({47, 9}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(238), array->at<std::uint32_t>({38, 2}));

    std::uint32_t value = 1859;
    array->at<std::uint32_t>({50, 90}) = value;
    CPPUNIT_ASSERT_EQUAL(value, array->at<std::uint32_t>({50, 90}));

    std::uint32_t value2 = 25464;
    array->at<std::uint32_t>({99, 99}) = value2;
    CPPUNIT_ASSERT_EQUAL(value2, array->at<std::uint32_t>({99, 99}));

    newSize = {2, 100, 100};
    array->resize(newSize, core::Type::UINT32, true);
    CPPUNIT_ASSERT(newSize == array->getSize());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(4 * 2 * 100 * 100), array->getSizeInBytes());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(0), array->at<std::uint32_t>({0, 0, 0}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(10), array->at<std::uint32_t>({0, 5, 0}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(999), array->at<std::uint32_t>({1, 99, 4}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(326), array->at<std::uint32_t>({0, 63, 1}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(947), array->at<std::uint32_t>({1, 73, 4}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(238), array->at<std::uint32_t>({0, 19, 1}));

    CPPUNIT_ASSERT_EQUAL(value, array->at<std::uint32_t>({0, 25, 45}));

    CPPUNIT_ASSERT_EQUAL(value2, array->at<std::uint32_t>({1, 99, 49}));

    std::uint32_t value3 = 2156;
    array->at<std::uint32_t>({0, 35, 48}) = value3;
    CPPUNIT_ASSERT_EQUAL(value3, array->at<std::uint32_t>({0, 35, 48}));

    std::uint32_t value4 = 105;
    array->at<std::uint32_t>({1, 99, 99}) = value4;
    CPPUNIT_ASSERT_EQUAL(value4, array->at<std::uint32_t>({1, 99, 99}));

    newSize.clear();

    newSize = {10, 100};

    array->resize(newSize, true);
    CPPUNIT_ASSERT(newSize == array->getSize());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(4), array->getElementSizeInBytes());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(4 * 10 * 100), array->getSizeInBytes());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(0), array->at<std::uint32_t>({0, 0}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(10), array->at<std::uint32_t>({0, 1}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(999), array->at<std::uint32_t>({9, 99}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(326), array->at<std::uint32_t>({6, 32}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(947), array->at<std::uint32_t>({7, 94}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(238), array->at<std::uint32_t>({8, 23}));

    newSize = {2, 10, 100};
    array->resize(newSize, core::Type::UINT16, true);
    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(2), array->getElementSizeInBytes());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(2 * 10 * 100 * 2), array->getSizeInBytes());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(0), array->at<std::uint32_t>({0, 0, 0}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(10), array->at<std::uint32_t>({0, 0, 1}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(999), array->at<std::uint32_t>({0, 9, 99}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(326), array->at<std::uint32_t>({0, 6, 32}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(947), array->at<std::uint32_t>({0, 7, 94}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(238), array->at<std::uint32_t>({0, 8, 23}));

    newSize = {10, 100};
    array->resize(newSize, true);
    CPPUNIT_ASSERT(newSize == array->getSize());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(2), array->getElementSizeInBytes());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(2 * 10 * 100), array->getSizeInBytes());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(0), array->at<std::uint32_t>({0, 0}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(10), array->at<std::uint32_t>({0, 2}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(326), array->at<std::uint32_t>({2, 65}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(238), array->at<std::uint32_t>({6, 47}));

    array->clear();
    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(0), array->getSizeInBytes());
}

//-----------------------------------------------------------------------------

void ArrayTest::copy()
{
    data::Array::sptr array = data::Array::New();

    data::Array::SizeType size = {10, 100};

    array->resize(size, core::Type::UINT32, true);
    auto arrayLock = array->dump_lock();
    CPPUNIT_ASSERT(array->getBuffer() != nullptr);

    std::uint32_t count = 0;
    auto iter           = array->begin<std::uint32_t>();
    auto iterEnd        = array->end<std::uint32_t>();

    for( ; iter != iterEnd ; ++iter)
    {
        *iter = count++;
    }

    data::Array::sptr deepCopyArray = data::Object::copy(array);
    auto deepCopyArrayLock          = deepCopyArray->dump_lock();

    // check deepCopy
    CPPUNIT_ASSERT_EQUAL(array->getElementSizeInBytes(), deepCopyArray->getElementSizeInBytes());
    CPPUNIT_ASSERT_EQUAL(array->getSizeInBytes(), deepCopyArray->getSizeInBytes());
    CPPUNIT_ASSERT(array->getStrides() == deepCopyArray->getStrides());
    CPPUNIT_ASSERT(array->getSize() == deepCopyArray->getSize());
    CPPUNIT_ASSERT_EQUAL(
        array->at<std::uint32_t>({0, 0}),
        deepCopyArray->at<std::uint32_t>({0, 0})
    );
    CPPUNIT_ASSERT_EQUAL(
        array->at<std::uint32_t>({0, 1}),
        deepCopyArray->at<std::uint32_t>({0, 1})
    );
    CPPUNIT_ASSERT_EQUAL(
        array->at<std::uint32_t>({9, 99}),
        deepCopyArray->at<std::uint32_t>({9, 99})
    );
    CPPUNIT_ASSERT_EQUAL(
        array->at<std::uint32_t>({6, 32}),
        deepCopyArray->at<std::uint32_t>({6, 32})
    );
    CPPUNIT_ASSERT_EQUAL(
        array->at<std::uint32_t>({7, 94}),
        deepCopyArray->at<std::uint32_t>({7, 94})
    );
    CPPUNIT_ASSERT_EQUAL(
        array->at<std::uint32_t>({8, 23}),
        deepCopyArray->at<std::uint32_t>({8, 23})
    );
    CPPUNIT_ASSERT_EQUAL(true, deepCopyArray->getIsBufferOwner());

    data::Array::csptr deepCopyArray2 = data::Object::copy(array);
    auto deepCopyArrayLock2           = deepCopyArray->dump_lock();

    CPPUNIT_ASSERT_EQUAL(array->getElementSizeInBytes(), deepCopyArray->getElementSizeInBytes());
    CPPUNIT_ASSERT_EQUAL(array->getSizeInBytes(), deepCopyArray->getSizeInBytes());
    CPPUNIT_ASSERT(array->getStrides() == deepCopyArray->getStrides());
    CPPUNIT_ASSERT(array->getSize() == deepCopyArray->getSize());
    CPPUNIT_ASSERT_EQUAL(
        array->at<std::uint32_t>({0, 0}),
        deepCopyArray->at<std::uint32_t>({0, 0})
    );
    CPPUNIT_ASSERT_EQUAL(
        array->at<std::uint32_t>({0, 1}),
        deepCopyArray->at<std::uint32_t>({0, 1})
    );
    CPPUNIT_ASSERT_EQUAL(
        array->at<std::uint32_t>({9, 99}),
        deepCopyArray->at<std::uint32_t>({9, 99})
    );
    CPPUNIT_ASSERT_EQUAL(
        array->at<std::uint32_t>({6, 32}),
        deepCopyArray->at<std::uint32_t>({6, 32})
    );
    CPPUNIT_ASSERT_EQUAL(
        array->at<std::uint32_t>({7, 94}),
        deepCopyArray->at<std::uint32_t>({7, 94})
    );
    CPPUNIT_ASSERT_EQUAL(
        array->at<std::uint32_t>({8, 23}),
        deepCopyArray->at<std::uint32_t>({8, 23})
    );
    CPPUNIT_ASSERT_EQUAL(true, deepCopyArray->getIsBufferOwner());
}

//-----------------------------------------------------------------------------

void ArrayTest::dumpLockTest()
{
    data::Array::SizeType size = {10, 100};
    data::Array::sptr array    = data::Array::New();
    array->resize({12, 15}, core::Type::INT16, true);

    CPPUNIT_ASSERT_THROW(array->getBuffer(), data::Exception);

    auto lock = array->dump_lock();
    CPPUNIT_ASSERT_NO_THROW(array->getBuffer());
}

//-----------------------------------------------------------------------------

void ArrayTest::bufferAccessTest()
{
    // test getBuffer
    data::Array::sptr array = data::Array::New();

    data::Array::SizeType size = {10, 100};

    array->resize(size, core::Type::UINT32, true);

    {
        CPPUNIT_ASSERT_THROW(array->getBuffer(), core::Exception);
        CPPUNIT_ASSERT_THROW(array->begin(), core::Exception);
        CPPUNIT_ASSERT_THROW(array->end(), core::Exception);
    }

    const auto lock = array->dump_lock();
    {
        // Check that the iterator properly lock the buffer
        CPPUNIT_ASSERT_NO_THROW(array->begin());
        CPPUNIT_ASSERT_NO_THROW(array->getBuffer());
    }

    std::uint32_t count = 0;
    for(auto& iter : array->range<std::uint32_t>())
    {
        iter = count++;
    }

    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(1000), count);
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(0), array->at<std::uint32_t>({0, 0}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(10), array->at<std::uint32_t>({0, 1}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(99), array->at<std::uint32_t>({9, 9}));

    CPPUNIT_ASSERT_THROW(array->at<std::uint32_t>({10, 0}), data::Exception);
    CPPUNIT_ASSERT_THROW(array->at<std::uint32_t>({0, 100}), data::Exception);

    {
        auto itr = array->begin<std::uint32_t>();
        CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(0), *itr);
        itr++;
        CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(1), *itr);
        ++itr;
        CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(2), *itr);
        itr += 5;
        CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(7), *itr);
        --itr;
        CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(6), *itr);
        itr -= 2;
        CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(4), *itr);

        std::uint32_t val = 4;
        const auto endItr = array->end<std::uint32_t>();
        while(itr != endItr)
        {
            CPPUNIT_ASSERT_EQUAL(val, *itr);
            CPPUNIT_ASSERT_NO_THROW(++itr);
            ++val;
        }

        CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(1000), val);
    }
    {
        auto itr = array->cbegin<std::uint32_t>();
        CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(0), *itr);
        itr++;
        CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(1), *itr);
        ++itr;
        CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(2), *itr);
        itr += 5;
        CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(7), *itr);
        --itr;
        CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(6), *itr);
        itr -= 2;
        CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(4), *itr);

        std::uint32_t val = 4;
        const auto endItr = array->end<std::uint32_t>();
        while(itr != endItr)
        {
            CPPUNIT_ASSERT_EQUAL(val, *itr);
            CPPUNIT_ASSERT_NO_THROW(++itr);
            ++val;
        }
    }
}

//-----------------------------------------------------------------------------

void ArrayTest::constArrayTest()
{
    data::Array::sptr array = data::Array::New();

    data::Array::SizeType size = {10, 100};

    array->resize(size, core::Type::UINT32, true);
    auto lock = array->dump_lock();

    std::uint32_t count = 0;
    auto iter           = array->begin<std::uint32_t>();
    for( ; iter != array->end<std::uint32_t>() ; ++iter)
    {
        *iter = count++;
    }

    data::Array::csptr array2 = data::Object::copy(array);
    const auto lock2          = array2->dump_lock();

    CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(4 * 10 * 100), array->getSizeInBytes());
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(0), array->at<std::uint32_t>({0, 0}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(0), array->at<std::uint32_t>(0));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(10), array->at<std::uint32_t>({0, 1}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(10), array->at<std::uint32_t>(10));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(999), array->at<std::uint32_t>({9, 99}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(999), array->at<std::uint32_t>(999));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(326), array->at<std::uint32_t>({6, 32}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(326), array->at<std::uint32_t>(326));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(947), array->at<std::uint32_t>({7, 94}));
    CPPUNIT_ASSERT_EQUAL(static_cast<std::uint32_t>(238), array->at<std::uint32_t>({8, 23}));

    auto it1        = array->begin<std::uint32_t>();
    auto it2        = array2->begin<std::uint32_t>();
    const auto end1 = array->end<std::uint32_t>();
    const auto end2 = array2->end<std::uint32_t>();

    while(it1 != end1 && it2 != end2)
    {
        CPPUNIT_ASSERT_EQUAL(*it1, *it2);
        CPPUNIT_ASSERT_NO_THROW(++it1);
        CPPUNIT_ASSERT_NO_THROW(++it2);
    }
}

//-----------------------------------------------------------------------------

void ArrayTest::emptyIteratorTest()
{
    data::Array::sptr array = data::Array::New();
    array->resize({10, 100}, core::Type::UINT32);
    auto lock = array->dump_lock();

    std::uint32_t count = 0;
    auto iterForFilling = array->begin<std::uint32_t>();
    const auto end      = array->end<std::uint32_t>();

    for( ; iterForFilling != end ; ++iterForFilling)
    {
        *iterForFilling = count++;
    }

    auto iter = array->begin<std::uint32_t>();

    data::Array::iterator<std::uint32_t> maxIter;

    std::uint32_t maxValue = *iter;
    for( ; iter != end ; ++iter)
    {
        if(*iter > maxValue)
        {
            maxIter  = iter;
            maxValue = *iter;
        }
    }

    CPPUNIT_ASSERT_EQUAL(*maxIter, count - 1);
}

//------------------------------------------------------------------------------

void ArrayTest::equalityTest()
{
    auto array1 = data::Array::New();
    auto array2 = data::Array::New();

    CPPUNIT_ASSERT(*array1 == *array2);

    // Fill array1
    const data::Array::SizeType size {10, 10};

    array1->resize(size, core::Type::UINT32, true);
    auto lock = array1->dump_lock();

    std::uint32_t count = 0;
    // NOLINTNEXTLINE(modernize-loop-convert)
    for(auto it = array1->begin<std::uint32_t>(), end = array1->end<std::uint32_t>() ; it != end ; ++it)
    {
        *it = count++;
    }

    CPPUNIT_ASSERT(*array1 != *array2);

    // Fill array2
    array2->resize(size, core::Type::UINT32, true);
    auto lock2 = array2->dump_lock();

    count = 666;
    // NOLINTNEXTLINE(modernize-loop-convert)
    for(auto it = array2->begin<std::uint32_t>(), end = array2->end<std::uint32_t>() ; it != end ; ++it)
    {
        *it = count++;
    }

    CPPUNIT_ASSERT(*array1 != *array2);

    count = 0;
    // NOLINTNEXTLINE(modernize-loop-convert)
    for(auto it = array2->begin<std::uint32_t>(), end = array2->end<std::uint32_t>() ; it != end ; ++it)
    {
        *it = count++;
    }

    CPPUNIT_ASSERT(*array1 == *array2);

    // Test also deepcopy, just for fun
    auto array3 = data::Array::New();
    array3->deepCopy(array1);
    CPPUNIT_ASSERT(*array1 == *array3);
}

//------------------------------------------------------------------------------

void ArrayTest::swapTest()
{
    auto array1 = data::Array::New();
    array1->resize({3}, core::Type::UINT32, true);
    auto lock1         = array1->dump_lock();
    std::uint8_t count = 1;
    // NOLINTNEXTLINE(modernize-loop-convert)
    for(auto it = array1->begin<std::uint32_t>(), end = array1->end<std::uint32_t>() ; it != end ; ++it)
    {
        *it = count++;
    }

    auto array2 = data::Array::New();
    array2->resize({6}, core::Type::INT16, true);
    auto lock2 = array2->dump_lock();
    // NOLINTNEXTLINE(modernize-loop-convert)
    for(auto it = array2->begin<std::int16_t>(), end = array2->end<std::int16_t>() ; it != end ; ++it)
    {
        *it = count++;
    }

    count = 1;
    std::size_t i = 0;
    for(auto it = array1->begin<std::uint32_t>(), end = array1->end<std::uint32_t>() ; it != end ; ++it, ++i)
    {
        CPPUNIT_ASSERT_MESSAGE("i=" + std::to_string(i), *it == count++);
    }

    i = 0;
    for(auto it = array2->begin<std::int16_t>(), end = array2->end<std::int16_t>() ; it != end ; ++it, ++i)
    {
        CPPUNIT_ASSERT_MESSAGE("i=" + std::to_string(i), *it == count++);
    }

    array1->swap(array2);

    count = 1;
    i     = 0;
    for(auto it = array2->begin<std::uint32_t>(), end = array2->end<std::uint32_t>() ; it != end ; ++it, ++i)
    {
        CPPUNIT_ASSERT_EQUAL_MESSAGE("i=" + std::to_string(i), static_cast<std::uint32_t>(count++), *it);
    }

    i = 0;
    for(auto it = array1->begin<std::int16_t>(), end = array1->end<std::int16_t>() ; it != end ; ++it, ++i)
    {
        CPPUNIT_ASSERT_EQUAL_MESSAGE("i=" + std::to_string(i), static_cast<std::int16_t>(count++), *it);
    }
}

//------------------------------------------------------------------------------

void ArrayTest::resizeNonOwnerTest()
{
    auto array   = data::Array::New();
    auto* buffer = new std::uint32_t[3];
    array->setBuffer(buffer, false, {3}, core::Type::UINT32);
    CPPUNIT_ASSERT_THROW(array->resize({6}, core::Type::UINT32, true), data::Exception);
    delete[] buffer;
}

//------------------------------------------------------------------------------

void ArrayTest::setBufferObjectNullThenResizeTest()
{
    auto array = data::Array::New();
    /* TODO: fix crash
       array->setBufferObject(nullptr);
       array->resize({2}, core::Type::UINT8);
     */
}

//------------------------------------------------------------------------------

void ArrayTest::atTest()
{
    auto array = data::Array::New();
    array->resize({3}, core::Type::UINT32, true);

    for(std::uint32_t i = 0 ; i < 3 ; i++)
    {
        array->at<std::uint32_t>(i) = i + 1;
    }

    for(std::uint32_t i = 0 ; i < 3 ; i++)
    {
        CPPUNIT_ASSERT_EQUAL(i + 1, array->at<std::uint32_t>(i));
    }
}

//-----------------------------------------------------------------------------

} // namespace sight::data::ut
