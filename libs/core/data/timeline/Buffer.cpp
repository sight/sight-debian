/************************************************************************
 *
 * Copyright (C) 2009-2022 IRCAD France
 * Copyright (C) 2012-2016 IHU Strasbourg
 *
 * This file is part of Sight.
 *
 * Sight is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Sight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Sight. If not, see <https://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

#include "data/timeline/Buffer.hpp"

#include <core/memory/exception/Memory.hpp>

namespace sight::data::timeline
{

//-----------------------------------------------------------------------------

Buffer::Buffer(
    core::HiResClock::HiResClockType timestamp,
    BufferDataType buffer,
    std::size_t size,
    DeleterType d
) :
    Object(timestamp),
    m_size(size),
    m_buffer(buffer),
    m_deleter(std::move(d))
{
}

//-----------------------------------------------------------------------------

Buffer::~Buffer()
{
    if(m_deleter)
    {
        m_deleter(m_buffer);
    }
}

//-----------------------------------------------------------------------------

void Buffer::deepCopy(const data::timeline::Object& other)
{
    Object::deepCopy(other);

    const auto& otherObject = static_cast<const Buffer&>(other);
    memcpy(m_buffer, otherObject.m_buffer, m_size);
}

//------------------------------------------------------------------------------

bool Buffer::operator==(const Buffer& other) const noexcept
{
    if(m_size != other.m_size)
    {
        return false;
    }

    if(m_buffer != other.m_buffer && std::memcmp(m_buffer, other.m_buffer, m_size) != 0)
    {
        return false;
    }

    // Super class last
    return Object::operator==(other);
}

//------------------------------------------------------------------------------

bool Buffer::operator!=(const Buffer& other) const noexcept
{
    return !(*this == other);
}

} // namespace sight::data::timeline
