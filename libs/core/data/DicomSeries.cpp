/************************************************************************
 *
 * Copyright (C) 2009-2023 IRCAD France
 * Copyright (C) 2012-2020 IHU Strasbourg
 *
 * This file is part of Sight.
 *
 * Sight is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Sight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Sight. If not, see <https://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

#include "data/DicomSeries.hpp"

#include <core/memory/stream/in/Raw.hpp>

#include <data/Exception.hpp>
#include <data/registry/macros.hpp>

#include <filesystem>

SIGHT_REGISTER_DATA(sight::data::DicomSeries)

namespace sight::data
{

DicomSeries::DicomSeries(data::Object::Key _key) :
    Series(_key)
{
}

//------------------------------------------------------------------------------

void DicomSeries::shallowCopy(const Object::csptr& source)
{
    const auto& other = dynamicConstCast(source);

    SIGHT_THROW_EXCEPTION_IF(
        Exception(
            "Unable to copy " + (source ? source->getClassname() : std::string("<NULL>"))
            + " to " + getClassname()
        ),
        !bool(other)
    );

    m_numberOfInstances   = other->m_numberOfInstances;
    m_dicomContainer      = other->m_dicomContainer;
    m_SOPClassUIDs        = other->m_SOPClassUIDs;
    m_computedTagValues   = other->m_computedTagValues;
    m_firstInstanceNumber = other->m_firstInstanceNumber;

    BaseClass::shallowCopy(other);
}

//------------------------------------------------------------------------------

void DicomSeries::deepCopy(const Object::csptr& source, const std::unique_ptr<DeepCopyCacheType>& cache)
{
    const auto& other = dynamicConstCast(source);

    SIGHT_THROW_EXCEPTION_IF(
        Exception(
            "Unable to copy " + (source ? source->getClassname() : std::string("<NULL>"))
            + " to " + getClassname()
        ),
        !bool(other)
    );

    m_numberOfInstances   = other->m_numberOfInstances;
    m_SOPClassUIDs        = other->m_SOPClassUIDs;
    m_computedTagValues   = other->m_computedTagValues;
    m_firstInstanceNumber = other->m_firstInstanceNumber;

    m_dicomContainer.clear();
    for(const auto& elt : other->m_dicomContainer)
    {
        const core::memory::BufferObject::sptr& bufferSrc = elt.second;
        core::memory::BufferObject::Lock lockerSource(bufferSrc);

        if(!bufferSrc->isEmpty())
        {
            auto bufferDest = core::memory::BufferObject::New(true);
            core::memory::BufferObject::Lock lockerDest(bufferDest);

            bufferDest->allocate(bufferSrc->getSize());

            char* buffDest = static_cast<char*>(lockerDest.getBuffer());
            char* buffSrc  = static_cast<char*>(lockerSource.getBuffer());
            std::copy(buffSrc, buffSrc + bufferSrc->getSize(), buffDest);

            m_dicomContainer[elt.first] = bufferDest;
        }
    }

    BaseClass::deepCopy(other, cache);
}

//------------------------------------------------------------------------------

void DicomSeries::addDicomPath(std::size_t _instanceIndex, const std::filesystem::path& _path)
{
    SIGHT_THROW_EXCEPTION_IF(
        std::filesystem::filesystem_error(
            std::string("Dicom path can not be found: "),
            _path,
            std::make_error_code(
                std::errc::no_such_file_or_directory
            )
        ),
        !std::filesystem::exists(_path)
    );

    auto buffer         = core::memory::BufferObject::New(true);
    const auto buffSize = std::filesystem::file_size(_path);
    buffer->setIStreamFactory(
        std::make_shared<core::memory::stream::in::Raw>(_path),
        static_cast<core::memory::BufferObject::SizeType>(buffSize),
        _path,
        core::memory::RAW
    );
    m_dicomContainer[_instanceIndex] = buffer;
}

//------------------------------------------------------------------------------

void DicomSeries::addBinary(std::size_t _instanceIndex, const core::memory::BufferObject::sptr& _buffer)
{
    m_dicomContainer[_instanceIndex] = _buffer;
}

//------------------------------------------------------------------------------

bool DicomSeries::isInstanceAvailable(std::size_t _instanceIndex) const
{
    const auto& dicomContainerIter = m_dicomContainer.find(_instanceIndex);
    return dicomContainerIter != m_dicomContainer.end();
}

//------------------------------------------------------------------------------

void DicomSeries::addSOPClassUID(const std::string& _sopClassUID)
{
    m_SOPClassUIDs.insert(_sopClassUID);
}

//------------------------------------------------------------------------------

void DicomSeries::addComputedTagValue(const std::string& _tagName, const std::string& _value)
{
    m_computedTagValues[_tagName] = _value;
}

//------------------------------------------------------------------------------

bool DicomSeries::hasComputedValues(const std::string& _tagName) const
{
    return m_computedTagValues.find(_tagName) != m_computedTagValues.end();
}

//------------------------------------------------------------------------------

bool DicomSeries::operator==(const DicomSeries& other) const noexcept
{
    if(m_numberOfInstances != other.m_numberOfInstances
       || m_SOPClassUIDs != other.m_SOPClassUIDs
       || m_computedTagValues != other.m_computedTagValues
       || m_firstInstanceNumber != other.m_firstInstanceNumber
       || !core::tools::is_equal(m_dicomContainer, other.m_dicomContainer))
    {
        return false;
    }

    // Super class last
    return BaseClass::operator==(other);
}

//------------------------------------------------------------------------------

bool DicomSeries::operator!=(const DicomSeries& other) const noexcept
{
    return !(*this == other);
}

} // namespace sight::data
