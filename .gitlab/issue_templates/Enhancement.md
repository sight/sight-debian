### Description

_Describe briefly the existing feature and what is the enhancement, use cases, benefits, and goals_

### Proposal

_Optional section to give some functional or technical hints_

### Functional specifications

_Workflow, UX/UI design, screenshots, etc..._

### Technical specifications

_Details of the implementation_

### Test plan

_Describe how you will verify that the implementation fulfils the specifications_

/label ~"Type::enhancement" ~"Priority::medium"
