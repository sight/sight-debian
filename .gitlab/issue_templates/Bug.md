### Description

_Summarize the bug encountered concisely_

### Steps to reproduce

_How one can reproduce the issue - this is very important._
_If the bug depends on a particular environment or platform, please give all relevant details._
_Paste any relevant logs. Please use code blocks (```) to format these._

### Proposal

_Optional section to give some functional or technical hints_

### Functional specifications

_If there is any change to the workflow, UX/UI design, screenshots, etc..., please describe it here_

### Technical specifications

_Details of the implementation of the fix_

### Test plan

_Describe how you will verify that the implementation fulfils the specifications_

/label ~"Type::bugfix" ~"Priority::medium"
