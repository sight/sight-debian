### Description

_Describe the existing code and why there is a need for a refactor, benefits, and goals_

### Proposal

_Optional section to give some functional or technical hints_

### Functional specifications

_Usually a refactor does not alter functional specifications,_
_but if there is any change to the workflow, UX/UI design, screenshots, etc..., please describe it here_

### Technical specifications

_Details of the implementation_

### Test plan

_Describe how you will verify that the implementation fulfils the specifications_

/label ~"Type::refactor" ~"Priority::medium"
