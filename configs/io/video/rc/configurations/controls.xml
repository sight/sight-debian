<!--
    Standard video widget
    It provides a toolbar for the standard play/pause/stop/loop actions.
    It includes a selector to choose the video source (device/file/stream) and the grabber.
    The camera resolution is handled through preferences and a selector.
    There are several input and output channels (int/double/bool/enum) to specify capture options
    or notify that some options have changed.
    The config outputs a timeline, and does not do any synchronisation.
    This config aims to be generic fo all kind of videos. As a result, all kind of specific
    video processing are not expected here.

Inout:
   camera  [sight::data::Camera]: camera used to display video.
   frameTL  [sight::data::FrameTL]: timeline where to extract the video frames.

ui elements:
   TOOLBAR: editor wid of a toolbar in which the play/pause/stop/loop buttons will be displayed.

parameters:
    videoSupport (optional, default="true") : indicates if the "file" video source is allowed or not
    playShortCut (optional, default="space") : indicates the shortcut to use to play the video
    pauseShortCut (optional, default="space") : indicates the shortcut to use to pause the video
    stopShortCut (optional, default="s") : indicates the shortcut to use to stop the video

channels - out (optional):

    notificationChannel:         channel to signal notifications to display them on screen to user (successes/failures/....)
    cameraStartedChannelOut:     channel on which a signal is triggered when the camera is started.
    cameraPausedChannelOut:      channel on which a signal is triggered when the camera is paused.
    cameraResumePauseChannelOut: channel on which a signal is triggered when the camera pause is resumed.
    cameraStoppedChannelOut:     channel on which a signal is triggered when the camera is stopped.
    loopModeToggleChannelOut:    channel on which a signal is triggered when the camera loop mode is toggled.

    parameterChannelOut(ui::base::parameter_t, std::string): channel on which a signal is triggered when a named grabber parameter is changed.

    videoPositionParameterOut: channel on which a signal is triggered when the position in the video is modified during playing.
    videoDurationParameterOut: channel on which a signal is triggered when the duration of the video is modified.

    configuredFileOut:   channel on which a signal is triggered the user selects a file as the video source.
    configuredStreamOut: channel on which a signal is triggered the user selects a stream as the video source.
    configuredDeviceOut: channel on which a signal is triggered when the user selects a device as the video source.

channels - in (optional):

    parameterChannelIn(ui::base::parameter_t, std::string) : channel on which a signal is expected to notify the grabber that a named parameter is changed.
    addROICenterChannelIn(sight::data::Point):        channel on which a signal is expected to notify the grabber that a new point has been selected.
    removeROICenterChannelIn(sight::data::Point):     channel on which a signal is expected to notify the grabber that a point has been remove.

Usage:

        <service uid="..." type="sight::service::SConfigController">
            <appConfig id="sight::config::io::video::controls" />
            <inout group="data">
                <key name="camera" uid="..." />
                <key name="frameTL" uid="..." />
            </inout>
            <parameter replace="TOOLBAR"                        by="..." />
            <parameter replace="videoSupport"                   by="..." />
            <parameter replace="playShortCut"                   by="..." />
            <parameter replace="pauseShortCut"                  by="..." />
            <parameter replace="stopShortCut"                   by="..." />
            <parameter replace="notificationChannel"            by="..." />
            <parameter replace="cameraStartedChannelOut"        by="..." />
            <parameter replace="cameraPausedChannelOut"         by="..." />
            <parameter replace="cameraResumePauseChannelOut"    by="..." />
            <parameter replace="cameraStoppedChannelOut"        by="..." />
            <parameter replace="loopModeToggleChannelOut"       by="..." />
            <parameter replace="parameterChannelIn"             by="..." />
            <parameter replace="parameterChannelOut"            by="..." />
            <parameter replace="configuredFileOut"              by="..." />
            <parameter replace="configuredStreamOut"            by="..." />
            <parameter replace="configuredDeviceOut"            by="..." />
            <parameter replace="addROICenterChannelIn"          by="..." />

        </service>
-->
<extension implements="sight::service::extension::AppConfig" >
    <id>sight::config::io::video::controls</id>
    <parameters>
        <param name="camera" />
        <param name="frameTL" />

        <param name="TOOLBAR" />
        <param name="videoSupport" default="true" />
        <param name="playShortCut" default="space" />
        <param name="pauseShortCut" default="space" />
        <param name="stopShortCut" default="s" />

        <param name="notificationChannel" default="notificationChannel" />

        <param name="cameraStartedChannelOut" default="cameraStartedChannelOut" />
        <param name="cameraPausedChannelOut" default="cameraPausedChannelOut" />
        <param name="cameraResumePauseChannelOut" default="cameraResumePauseChannelOut" />
        <param name="cameraStoppedChannelOut" default="cameraStoppedChannelOut" />
        <param name="loopModeToggleChannelOut" default="loopModeToggleChannelOut" />

        <param name="parameterChannelIn" default="parameterChannelIn" />
        <param name="parameterChannelOut" default="parameterChannelOut" />

        <param name="videoPositionParameterIn" default="videoPositionParameterIn" />
        <param name="videoPositionParameterOut" default="videoPositionParameterOut" />
        <param name="videoDurationParameterOut" default="videoDurationParameterOut" />

        <param name="configuredFileOut" default="configuredFileOut" />
        <param name="configuredStreamOut" default="configuredStreamOut" />
        <param name="configuredDeviceOut" default="configuredDeviceOut" />

        <param name="addROICenterChannelIn" default="addROICenterChannelIn" />
        <param name="removeROICenterChannelIn" default="removeROICenterChannelIn" />

        <param name="optimizeGrabberIn" default="optimizeGrabberIn" />

    </parameters>
    <config>

        <object uid="${camera}" type="sight::data::Camera" src="ref" />
        <object uid="${frameTL}" type="sight::data::FrameTL" src="ref" />

        <service uid="containerViewSrv" type="sight::module::ui::base::SView" >
            <gui>
                <toolBar />
                <layout type="sight::ui::base::LineLayoutManager" >
                    <orientation value="horizontal" />
                </layout>
            </gui>
            <registry>
                <parent wid="${TOOLBAR}" />
                <toolBar sid="toolbarViewSrv" start="true" />
            </registry>
        </service>

        <service uid="videoSelectorSrv" type="sight::module::ui::qt::video::SCamera" >
            <inout key="camera" uid="${camera}" />
            <videoSupport>${videoSupport}</videoSupport>
        </service>

        <service uid="toolbarViewSrv" type="sight::module::ui::base::SToolBar" >
            <gui>
                <layout>
                    <editor/>
                    <menuItem name="Start" icon="sight::module::ui::flaticons/GreenStart.svg" shortcut="${playShortCut}"/>
                    <menuItem name="Pause" icon="sight::module::ui::flaticons/OrangePause.svg" shortcut="${pauseShortCut}"/>
                    <menuItem name="Play" icon="sight::module::ui::flaticons/GreenStart.svg" shortcut="${pauseShortCut}"/>
                    <menuItem name="Stop" icon="sight::module::ui::flaticons/RedStop.svg" shortcut="${stopShortCut}"/>
                    <menuItem name="Loop" icon="sight::module::ui::flaticons/OrangeLoop.svg" style="check" />
                </layout>
            </gui>
            <registry>
                <editor sid="videoSelectorSrv" start="true" />
                <menuItem sid="startVideoAct" start="true" />
                <menuItem sid="pauseVideoAct" start="true" />
                <menuItem sid="resumeVideoAct" start="true" />
                <menuItem sid="stopVideoAct" start="true" />
                <menuItem sid="loopVideoAct" start="true" />
            </registry>
        </service>

        <service uid="grabberProxySrv" type="sight::module::io::video::SGrabberProxy" >
            <in key="camera" uid="${camera}" />
            <inout key="frameTL" uid="${frameTL}" />
        </service>

        <!-- ******************************* Actions ****************************** -->

        <!-- Start the frame grabber -->
        <service uid="startVideoAct" type="sight::module::ui::base::SAction" />

        <!-- Pause the frame grabber -->
        <service uid="pauseVideoAct" type="sight::module::ui::base::SAction" >
            <state visible="false" />
        </service>

        <!-- Resume the frame grabber -->
        <service uid="resumeVideoAct" type="sight::module::ui::base::SAction" >
            <state visible="false" />
        </service>

        <!-- Stop the frame grabber -->
        <service uid="stopVideoAct" type="sight::module::ui::base::SAction" >
            <state enabled="false" />
        </service>

        <!-- Loop the frame grabber -->
        <service uid="loopVideoAct" type="sight::module::ui::base::SAction" >
            <state enabled="false" />
        </service>

        <!-- ******************************* Connections ******************************* -->

        <connect>
            <signal>startVideoAct/clicked</signal>
            <slot>grabberProxySrv/startCamera</slot>
        </connect>

        <connect channel="${cameraPausedChannelOut}">
            <signal>pauseVideoAct/clicked</signal>
            <slot>grabberProxySrv/pauseCamera</slot>
            <slot>resumeVideoAct/show</slot>
            <slot>pauseVideoAct/hide</slot>
        </connect>

        <connect channel="${cameraResumePauseChannelOut}">
            <signal>resumeVideoAct/clicked</signal>
            <slot>grabberProxySrv/pauseCamera</slot>
            <slot>resumeVideoAct/hide</slot>
            <slot>pauseVideoAct/show</slot>
        </connect>

        <connect>
            <signal>stopVideoAct/clicked</signal>
            <slot>grabberProxySrv/stopCamera</slot>
            <slot>startVideoAct/show</slot>
            <slot>resumeVideoAct/hide</slot>
            <slot>pauseVideoAct/hide</slot>
            <slot>stopVideoAct/disable</slot>
            <slot>loopVideoAct/disable</slot>
            <slot>loopVideoAct/uncheck</slot>
        </connect>

        <connect channel="${loopModeToggleChannelOut}">
            <signal>loopVideoAct/clicked</signal>
            <slot>grabberProxySrv/loopVideo</slot>
        </connect>

        <connect>
            <signal>videoSelectorSrv/configuredCameras</signal>
            <slot>grabberProxySrv/reconfigure</slot>
        </connect>

        <connect>
            <signal>videoSelectorSrv/configuredStream</signal>
            <signal>videoSelectorSrv/configuredDevice</signal>
            <slot>startVideoAct/update</slot>
        </connect>

        <connect channel="${configuredFileOut}">
            <signal>videoSelectorSrv/configuredFile</signal>
        </connect>

        <connect channel="${configuredStreamOut}">
            <signal>videoSelectorSrv/configuredStream</signal>
        </connect>

        <connect channel="${configuredDeviceOut}">
            <signal>videoSelectorSrv/configuredDevice</signal>
        </connect>

        <connect channel="${cameraStartedChannelOut}" >
            <signal>grabberProxySrv/cameraStarted</signal>
            <slot>pauseVideoAct/show</slot>
            <slot>startVideoAct/hide</slot>
            <slot>stopVideoAct/enable</slot>
            <slot>loopVideoAct/enable</slot>
        </connect>

        <connect>
            <signal>camera/idModified</signal>
            <slot>grabberProxySrv/stopCamera</slot>
        </connect>

        <connect channel="${cameraStoppedChannelOut}" >
            <signal>grabberProxySrv/cameraStopped</signal>
        </connect>

        <connect>
            <signal>camera/modified</signal>
            <slot>startVideoAct/enable</slot>
            <slot>stopVideoAct/update</slot>
        </connect>

        <connect channel="${parameterChannelIn}" >
            <slot>grabberProxySrv/setParameter</slot>
        </connect>

        <connect channel="${parameterChannelOut}" >
            <signal>grabberProxySrv/parameterChanged</signal>
        </connect>

        <connect channel="${videoPositionParameterIn}">
            <slot>grabberProxySrv/setPositionVideo</slot>
        </connect>

        <connect channel="${videoPositionParameterOut}">
            <signal>grabberProxySrv/positionModified</signal>
        </connect>

        <connect channel="${videoDurationParameterOut}">
            <signal>grabberProxySrv/durationModified</signal>
        </connect>

        <connect channel="${notificationChannel}">
            <signal>grabberProxySrv/notified</signal>
        </connect>

        <connect channel="${addROICenterChannelIn}">
            <slot>grabberProxySrv/addROICenter</slot>
        </connect>

        <connect channel="${removeROICenterChannelIn}">
            <slot>grabberProxySrv/removeROICenter</slot>
        </connect>

        <connect channel="${optimizeGrabberIn}">
            <slot>grabberProxySrv/optimize</slot>
        </connect>

        

        <start uid="containerViewSrv" />
        <start uid="grabberProxySrv" />

    </config>
</extension>
