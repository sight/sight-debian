<extension implements="sight::service::extension::AppConfig">
    <id>2DNegato</id>
    <parameters>
        <param name="WID_PARENT" />
        <param name="image" />
        <param name="landmarks" />
        <param name="patient_name" default="" />
        <param name="orientation" default="axial" /> <!-- axial, frontal, sagittal -->
        <param name="CrossTypeChannel" default="crossTypeChannel" />
        <param name="PickingChannel" default="pickingChannel" />
    </parameters>
    <config>
        <object uid="${image}" type="sight::data::Image" src="ref" />
        <object uid="tf" type="sight::data::TransferFunction" />
        <object uid="${landmarks}" type="sight::data::Landmarks" src="ref" />
        <object uid="snapshot" type="sight::data::Image" />

        <service uid="mainView" type="sight::module::ui::base::SView">
            <gui>
                <layout type="sight::ui::base::OverlayLayoutManager">
                    <view />
                    <view x="0" y="0" height="55" width="100" />
                    <view x="0" y="-1" height="35" width="100%" />
                </layout>
            </gui>
            <registry>
                <parent wid="${WID_PARENT}" />
                <view sid="negato" start="true" />
                <view sid="negato_top_slider_view" start="true" />
                <view sid="negato_bottom_slider_view" start="true" />
            </registry>
        </service>

        <service uid="negato_top_slider_view" type="sight::module::ui::base::SView">
            <gui>
                <toolBar backgroundColor="#00000000" />
                <layout type="sight::ui::base::LineLayoutManager">
                    <orientation value="vertical" />
                </layout>
            </gui>
            <registry>
                <toolBar sid="topToolbarView" start="true" />
            </registry>
        </service>

        <service uid="topToolbarView" type="sight::module::ui::base::SToolBar">
            <gui>
                <layout>
                    <menuItem name="Snapshot" icon="sight::module::ui::flaticons/YellowPhoto.svg" shortcut="CTRL+S" />
                    <spacer />
                </layout>
            </gui>
            <registry>
                <menuItem sid="snapshotAct" start="true" />
            </registry>
        </service>

        <service uid="negato_bottom_slider_view" type="sight::module::ui::base::SView">
            <gui>
                <layout type="sight::ui::base::LineLayoutManager">
                    <orientation value="vertical" />
                    <spacer />
                    <view proportion="0" backgroundColor="#00000000" />
                </layout>
            </gui>
            <registry>
                <view sid="multiView_negato_bottom" start="true" />
            </registry>
        </service>

        <service uid="multiView_negato_bottom" type="sight::module::ui::base::SView">
            <gui>
                <layout type="sight::ui::base::LineLayoutManager">
                    <orientation value="horizontal" />
                    <view proportion="0" />
                </layout>
            </gui>
            <registry>
                <view sid="slider_negato" start="true" />
            </registry>
        </service>

        <!-- Save the snapshot image -->
        <service uid="snapshotAct" type="sight::module::ui::base::SAction" />

        <!-- Generic Scene Negato -->
        <!-- *************************** Begin generic scene *************************** -->

        <service uid="negato" type="sight::viz::scene3d::SRender">
            <scene>
                <background topColor="#000000" bottomColor="#000000" />

                <layer id="defaultLayer" order="1">
                    <adaptor uid="snapshotAdp" />
                    <adaptor uid="negatoCameraAdp" />
                    <adaptor uid="pickerInteractorAdp" />
                    <adaptor uid="negatoAdp" />
                    <adaptor uid="multiDistancesAdp" />
                    <adaptor uid="landmarksAdp" />
                </layer>
            </scene>
        </service>

        <service uid="snapshotAdp" type="sight::module::viz::scene3d::adaptor::SFragmentsInfo">
            <inout key="image" uid="snapshot" />
        </service>

        <service uid="negatoCameraAdp" type="sight::module::viz::scene3d::adaptor::SNegato2DCamera">
            <inout key="image" uid="${image}" />
            <inout key="tf" uid="tf" />
            <config priority="0" orientation="${orientation}" />
        </service>

        <service uid="pickerInteractorAdp" type="sight::module::viz::scene3d::adaptor::SPicker">
            <config queryMask="0x40000000" priority="1" />
        </service>

        <service uid="negatoAdp" type="sight::module::viz::scene3d::adaptor::SNegato2D" autoConnect="true">
            <in key="image" uid="${image}" />
            <in key="tf" uid="tf" />
            <config sliceIndex="${orientation}" filtering="none" />
        </service>

        <service uid="multiDistancesAdp" type="sight::module::viz::scene3dQt::adaptor::SImageMultiDistances" autoConnect="true">
            <inout key="image" uid="${image}" />
            <config fontSize="32" radius="4.5" queryFlags="0x40000000" priority="3" />
        </service>

        <service uid="landmarksAdp" type="sight::module::viz::scene3dQt::adaptor::SLandmarks" autoConnect="true">
            <inout key="landmarks" uid="${landmarks}" />
            <config priority="4" />
        </service>

        <!-- *************************** End generic scene *************************** -->

        <service uid="LockImageSrv" type="sight::module::memory::LockDumpSrv">
            <inout key="target" uid="${image}" />
        </service>

        <service uid="slider_negato" type="sight::module::ui::qt::image::SliceIndexPositionEditor" autoConnect="true">
            <inout key="image" uid="${image}" />
            <sliceIndex>${orientation}</sliceIndex>
        </service>

        <!-- Write the snapshot image -->
        <service uid="imageWriterSrv" type="sight::module::io::bitmap::SWriter">
            <in key="data" uid="snapshot" />
            <dialog policy="always" />
            <backends enable="all" mode="best"/>
        </service>

        <connect channel="${CrossTypeChannel}">
            <slot>MPRNegato/setCrossScale</slot>
        </connect>

        <connect channel="${PickingChannel}">
            <signal>pickerInteractorAdp/picked</signal>
        </connect>

        <!-- Slide view -->
        <connect>
            <signal>mainView/started</signal>
            <slot>negato_top_slider_view/show</slot>
            <slot>negato_bottom_slider_view/show</slot>
        </connect>

        <connect>
            <signal>snapshotAct/clicked</signal>
            <slot>imageWriterSrv/update</slot>
        </connect>

        <start uid="LockImageSrv" />
        <start uid="mainView" />

        <!-- negato adaptors -->
        <start uid="snapshotAdp" />
        <start uid="negatoCameraAdp" />
        <start uid="pickerInteractorAdp" />
        <start uid="negatoAdp" />
        <start uid="multiDistancesAdp" />
        <start uid="landmarksAdp" />
        <start uid="imageWriterSrv" />
    </config>
</extension>
