<extension implements="sight::service::extension::AppConfig">
    <id>calExtrinsicView</id>
    <parameters>
        <param name="WID_PARENT" />
        <param name="cameraSet" />
        <param name="camera1" />
        <param name="camera2" />
        <param name="calibrationInfo1" />
        <param name="calibrationInfo2" />
        <param name="camIndex" />
        <param name="preferencesModifiedProxy" />
    </parameters>
    <config>

        <!-- ******************************* Objects declaration ****************************** -->

        <!-- Displayed image pair. -->
        <object uid="frame1" type="sight::data::Image" />
        <object uid="frame2" type="sight::data::Image" />

        <!-- Second pair of images used for detection. Prevents the detector from slowing down the video. -->
        <object uid="frame1TL" type="sight::data::FrameTL" />
        <object uid="frame2TL" type="sight::data::FrameTL" />
        <object uid="${calibrationInfo1}" type="sight::data::CalibrationInfo" src="ref" />
        <object uid="${calibrationInfo2}" type="sight::data::CalibrationInfo" src="ref" />
        <object uid="${cameraSet}" type="sight::data::CameraSet" src="ref" />
        <object uid="${camera1}" type="sight::data::Camera" src="ref" />
        <object uid="${camera2}" type="sight::data::Camera" src="ref" />
        <object uid="exportExtrinsicMat" type="sight::data::Matrix4" src="deferred" />

        <!-- ******************************* UI declaration *********************************** -->

        <service uid="mainView" type="sight::module::ui::base::SView">
            <gui>
                <layout type="sight::ui::base::LineLayoutManager">
                    <orientation value="horizontal" />
                    <view proportion="2" />
                    <view proportion="7" />
                </layout>
            </gui>
            <registry>
                <parent wid="${WID_PARENT}" />
                <view sid="leftPanelView" start="true" />
                <view sid="videoView" start="true" />
            </registry>

        </service>

        <service uid="leftPanelView" type="sight::module::ui::base::SView" >
            <gui>
                <layout type="sight::ui::base::LineLayoutManager">
                    <orientation value="vertical" />
                    <view proportion="1" />
                    <view proportion="0" />
                </layout>
            </gui>
            <registry>
                <view sid="toolboxView" start="true" />
                <view sid="errorLabelSrv" start="true" />
            </registry>
        </service>

        <service uid="toolboxView" type="sight::module::ui::base::SView" >
            <gui>
                <toolBar/>
                <layout type="sight::ui::base::ToolboxLayoutManager">
                    <orientation value="vertical" />
                    <view proportion="1" caption="Calibration input" expanded="true" />
                    <view proportion="1" caption="Calibration result" expanded="true" />
                </layout>
            </gui>
            <registry>
                <toolBar sid="videoToolbarView" start="true" />
                <view sid="calibrationDataView" start="true" />
                <view sid="cameraInfoView" start="true" />
            </registry>
        </service>

        <service uid="videoToolbarView" type="sight::module::ui::base::SToolBar">
            <gui>
                <layout>
                    <menuItem name="Start" icon="sight::module::ui::flaticons/GreenStart.svg" shortcut="Space" />
                    <menuItem name="Pause" icon="sight::module::ui::flaticons/OrangePause.svg" shortcut="Space" />
                    <menuItem name="Play" icon="sight::module::ui::flaticons/GreenStart.svg" shortcut="Space" />
                    <menuItem name="Stop" icon="sight::module::ui::flaticons/RedStop.svg" />
                    <menuItem name="Loop" icon="sight::module::ui::flaticons/OrangeLoop.svg" style="check" />
                </layout>
            </gui>
            <registry>
                <menuItem sid="startVideoAct" start="true" />
                <menuItem sid="pauseVideoAct" start="true" />
                <menuItem sid="resumeVideoAct" start="true" />
                <menuItem sid="stopVideoAct" start="true" />
                <menuItem sid="loopVideoAct" start="true" />
            </registry>
        </service>

        <service uid="calibrationDataView" type="sight::module::ui::base::SView">
            <gui>
                <toolBar>
                    <toolBitmapSize height="24" width="24" />
                </toolBar>
                <layout type="sight::ui::base::CardinalLayoutManager">
                    <view align="center" />
                </layout>
            </gui>
            <registry>
                <toolBar sid="extrinsicCameraView" start="true" />
                <view sid="calibrationInfoEditorSrv" start="true" />
            </registry>
        </service>


        <service uid="cameraInfoView" type="sight::module::ui::base::SView">
            <gui>
                <layout type="sight::ui::base::LineLayoutManager">
                    <orientation value="vertical" />
                    <view caption="camera 1" />
                    <view caption="camera 2" />
                    <view caption="Extrinsic (1 to 2)" />
                </layout>
            </gui>
            <registry>
                <view sid="cameraInfo1Srv" start="true" />
                <view sid="cameraInfo2Srv" start="true" />
                <view sid="cameraSetInfoSrv" start="true" />
            </registry>
        </service>

        <service uid="extrinsicCameraView" type="sight::module::ui::base::SToolBar">
            <gui>
                <layout>
                    <editor/>
                    <menuItem name="Add" icon="sight::module::ui::flaticons/GreenPlus.svg"  shortcut="A" />
                    <menuItem name="Remove" icon="sight::module::ui::flaticons/RedMinus.svg" />
                    <menuItem name="Calibrate" icon="sight::module::ui::flaticons/YellowPhoto.svg" shortcut="Return" />
                    <menuItem name="Display" icon="sight::module::ui::flaticons/YellowFullscreen.svg" />
                    <menuItem name="Reset" icon="sight::module::ui::flaticons/RedReset.svg" />
                    <menuItem name="Export extrinsic matrix" icon="sight::module::ui::flaticons/BlueLoad.svg" />
                    <menuItem name="Save calibration images" icon="sight::module::ui::flaticons/BlueLoad.svg" />
                    <menuItem name="Load calibration images" icon="sight::module::ui::flaticons/BlueSave.svg" />
                </layout>
            </gui>
            <registry>
                <editor sid="detectionStatusSrv" start="true" />
                <menuItem sid="addAct" start="true" />
                <menuItem sid="removeAct" start="true" />
                <menuItem sid="goOpenCVAct" start="true" />
                <menuItem sid="displayImageAct" start="true" />
                <menuItem sid="resetAct" start="true" />
                <menuItem sid="exportExtrinsicAct" start="true" />
                <menuItem sid="saveInputsAct" start="true" />
                <menuItem sid="loadInputsAct" start="true" />
            </registry>
        </service>

        <service uid="videoView" type="sight::module::ui::base::SView">
            <gui>
                <layout type="sight::ui::base::OverlayLayoutManager">
                    <view />
                    <view x="0" y="-1" height="35" width="100%" />
                </layout>
            </gui>
            <registry>
                <view sid="genericSceneSrv" start="true" />
                <view sid="videoSliderView" start="true" />
            </registry>
        </service>

        <service uid="videoSliderView" type="sight::module::ui::base::SView">
            <gui>
                <layout type="sight::ui::base::LineLayoutManager">
                    <orientation value="vertical" />
                    <spacer />
                    <view proportion="0" />
                </layout>
            </gui>
            <registry>
                <view sid="videoSliderSrv" start="true" />
            </registry>
        </service>

        <!-- ************************************* Action ************************************ -->

        <service uid="addAct" type="sight::module::ui::base::SAction"/>
        <service uid="resetAct" type="sight::module::ui::base::SAction"/>
        <service uid="removeAct" type="sight::module::ui::base::SAction"/>
        <service uid="goOpenCVAct" type="sight::module::ui::base::SAction"/>
        <service uid="displayImageAct" type="sight::module::ui::base::SAction"/>

        <!-- Start the frame grabber -->
        <service uid="startVideoAct" type="sight::module::ui::base::SAction" />

        <!-- Pause the frame grabber -->
        <service uid="pauseVideoAct" type="sight::module::ui::base::SAction" >
            <state visible="false" />
        </service>

        <!-- Resume the frame grabber -->
        <service uid="resumeVideoAct" type="sight::module::ui::base::SAction" >
            <state visible="false" />
        </service>

        <!-- Stop the frame grabber -->
        <service uid="stopVideoAct" type="sight::module::ui::base::SAction" >
            <state enabled="false" />
        </service>

        <!-- Loop the frame grabber -->
        <service uid="loopVideoAct" type="sight::module::ui::base::SAction">
            <state enabled="false" />
        </service>

        <service uid="exportExtrinsicAct" type="sight::module::ui::base::SAction" />

        <service uid="saveInputsAct" type="sight::module::ui::base::SAction" >
            <sync>true</sync>
        </service>
        <service uid="loadInputsAct" type="sight::module::ui::base::SAction" >
            <sync>true</sync>
        </service>

        <!-- ******************************* Begin Generic Scene ******************************* -->

        <service uid="genericSceneSrv" type="sight::viz::scene3d::SRender" >
            <scene renderMode="sync">
                <background topColor="#000000" bottomColor="#000000" />

                <layer id="video1" order="1">
                    <viewport width="1.0" height="0.5" hAlign="left" vAlign="top"/>
                    <adaptor uid="video1Adp" />
                </layer>

                <layer id="video2" order="2">
                    <viewport width="1.0" height="0.5" hAlign="left" vAlign="bottom"/>
                    <adaptor uid="video2Adp" />
                </layer>
            </scene>
        </service>

        <service uid="video1Adp" type="sight::module::viz::scene3d::adaptor::SVideo" autoConnect="true" >
            <in key="image" uid="frame1" />
        </service>

        <service uid="video2Adp" type="sight::module::viz::scene3d::adaptor::SVideo" autoConnect="true" >
            <in key="image" uid="frame2" />
        </service>

        <!-- ************************************* Services ************************************ -->

        <service uid="videoSliderSrv" type="sight::module::ui::qt::video::SSlider" />

        <!-- Extracts images for the detection algorithm. -->
        <service uid="frameSynchronizerSrv"  type="sight::module::sync::SSynchronizer">
            <in group="frameTL">
                <key uid="frame1TL" />
                <key uid="frame2TL" />
            </in>
            <inout group="frames">
                <key uid="frame1" tl="0" />
                <key uid="frame2" tl="1" />
            </inout>
        </service>

        <service uid="extrinsicExporterSrv" type="sight::module::ui::base::io::SSelector" >
            <inout key="data" uid="exportExtrinsicMat" />
            <type mode="writer" />
            <selection mode="include" />
            <addSelection service="sight::module::io::matrix::Matrix4WriterService" />
        </service>

        <service uid="calibrationInfoEditorSrv" type="sight::module::ui::qt::calibration::SCalibrationInfoEditor" >
            <inout key="calInfo1" uid="${calibrationInfo1}" />
            <inout key="calInfo2" uid="${calibrationInfo2}" />
        </service>

        <service uid="detectionStatusSrv" type="sight::module::ui::qt::SStatus">
            <green>Point are visible</green>
            <orange>Calibration in progress</orange>
            <red>Points are NOT visible</red>
        </service>

        <service uid="errorLabelSrv" type="sight::module::ui::qt::STextStatus">
            <label>Reprojection error (RMSE)</label>
            <color>#D25252</color>
        </service>

        <service uid="chessboardDetectorSrv" type="sight::module::geometry::vision::SChessBoardDetector" worker="extr_chess_checker">
            <in group="image" autoConnect="true" >
                <key uid="frame1" />
                <key uid="frame2" />
            </in>
            <inout group="calInfo">
                <key uid="${calibrationInfo1}" />
                <key uid="${calibrationInfo2}" />
            </inout>
            <board width="CHESSBOARD_WIDTH" height="CHESSBOARD_HEIGHT" scale="CHESSBOARD_SCALE" />
        </service>

        <service uid="displayCalibrationInfoSrv" type="sight::module::ui::qt::calibration::SDisplayCalibrationInfo">
            <in key="calInfo1" uid="${calibrationInfo1}" />
            <in key="calInfo2" uid="${calibrationInfo2}" />
        </service>

        <service uid="videoGrabber1Srv" type="sight::module::io::video::SGrabberProxy">
            <in key="camera" uid="${camera1}" />
            <inout key="frameTL" uid="frame1TL" />
            <config>
                <gui title="Please select the first camera grabber" />
            </config>
        </service>

        <service uid="videoGrabber2Srv" type="sight::module::io::video::SGrabberProxy">
            <in key="camera" uid="${camera2}" />
            <inout key="frameTL" uid="frame2TL" />
            <config>
                <gui title="Please select the second camera grabber" />
            </config>
        </service>

        <service uid="openCVExtrinsicCalSrv" type="sight::module::geometry::vision::SOpenCVExtrinsic" worker="extr_chess_checker">
            <in key="calibrationInfo1" uid="${calibrationInfo1}" />
            <in key="calibrationInfo2" uid="${calibrationInfo2}" />
            <inout key="cameraSet" uid="${cameraSet}" />
            <out   key="matrix" uid="exportExtrinsicMat" />
            <camIndex>${camIndex}</camIndex>
            <board width="CHESSBOARD_WIDTH" height="CHESSBOARD_HEIGHT" squareSize="CHESSBOARD_SQUARE_SIZE" />
        </service>

        <service uid="cameraSetInfoSrv" type="sight::module::ui::qt::calibration::SCameraSetEditor" autoConnect="true">
            <in key="cameraSet" uid="${cameraSet}" />
            <camIndex>${camIndex}</camIndex>
        </service>

        <service uid="cameraInfo1Srv" type="sight::module::ui::qt::calibration::SCameraInformationEditor" autoConnect="true">
            <in key="camera" uid="${camera1}" />
        </service>

        <service uid="cameraInfo2Srv" type="sight::module::ui::qt::calibration::SCameraInformationEditor" autoConnect="true">
            <in key="camera" uid="${camera2}" />
        </service>

        <service uid="calibrationDataWriter1Srv" type="sight::module::io::vision::SCalibrationImagesWriter">
            <in key="data" uid="${calibrationInfo1}" />
            <format>.tiff</format>
        </service>

        <service uid="calibrationDataWriter2Srv" type="sight::module::io::vision::SCalibrationImagesWriter">
            <in key="data" uid="${calibrationInfo2}" />
            <format>.tiff</format>
        </service>

        <service uid="calibrationDataReader1Srv" type="sight::module::io::vision::SCalibrationInfoReader">
            <windowTitle>Load left images.</windowTitle>
            <inout key="data" uid="${calibrationInfo1}" />
            <board width="CHESSBOARD_WIDTH" height="CHESSBOARD_HEIGHT" scale="CHESSBOARD_SCALE" />
        </service>

        <service uid="calibrationDataReader2Srv" type="sight::module::io::vision::SCalibrationInfoReader">
            <windowTitle>Load right images.</windowTitle>
            <inout key="data" uid="${calibrationInfo2}" />
            <board width="CHESSBOARD_WIDTH" height="CHESSBOARD_HEIGHT" scale="CHESSBOARD_SCALE" />
        </service>

        <!-- ******************************* Connections ***************************************** -->

        <!-- Actions connections -->
        <connect>
            <signal>addAct/clicked</signal>
            <slot>chessboardDetectorSrv/recordPoints</slot>
        </connect>

        <connect>
            <signal>resetAct/clicked</signal>
            <slot>calibrationInfoEditorSrv/reset</slot>
        </connect>

        <connect>
            <signal>removeAct/clicked</signal>
            <slot>calibrationInfoEditorSrv/remove</slot>
        </connect>

        <connect>
            <signal>goOpenCVAct/clicked</signal>
            <slot>openCVExtrinsicCalSrv/update</slot>
            <slot>Status/changeToOrange</slot>
        </connect>

        <connect>
            <signal>displayImageAct/clicked</signal>
            <slot>calibrationInfoEditorSrv/getSelection</slot>
        </connect>

        <connect>
            <signal>startVideoAct/clicked</signal>
            <slot>videoGrabber1Srv/startCamera</slot>
            <slot>videoGrabber2Srv/startCamera</slot>
        </connect>

        <connect>
            <signal>pauseVideoAct/clicked</signal>
            <slot>videoGrabber1Srv/pauseCamera</slot>
            <slot>videoGrabber2Srv/pauseCamera</slot>
            <slot>resumeVideoAct/show</slot>
            <slot>pauseVideoAct/hide</slot>
        </connect>

        <connect>
            <signal>resumeVideoAct/clicked</signal>
            <slot>videoGrabber1Srv/pauseCamera</slot>
            <slot>videoGrabber2Srv/pauseCamera</slot>
            <slot>resumeVideoAct/hide</slot>
            <slot>pauseVideoAct/show</slot>
        </connect>

        <connect>
            <signal>stopVideoAct/clicked</signal>
            <slot>videoGrabber1Srv/stopCamera</slot>
            <slot>videoGrabber2Srv/stopCamera</slot>
            <slot>startVideoAct/show</slot>
            <slot>resumeVideoAct/hide</slot>
            <slot>pauseVideoAct/hide</slot>
            <slot>stopVideoAct/disable</slot>
            <slot>loopVideoAct/disable</slot>
            <slot>loopVideoAct/uncheck</slot>
        </connect>

        <connect>
            <signal>loopVideoAct/clicked</signal>
            <slot>videoGrabber1Srv/loopVideo</slot>
            <slot>videoGrabber2Srv/loopVideo</slot>
        </connect>

        <connect>
            <signal>exportExtrinsicAct/clicked</signal>
            <slot>extrinsicExporterSrv/update</slot>
        </connect>

        <connect>
            <signal>saveInputsAct/clicked</signal>
            <slot>calibrationDataWriter1Srv/openLocationDialog</slot>
            <slot>calibrationDataWriter1Srv/update</slot>
            <slot>calibrationDataWriter2Srv/openLocationDialog</slot>
            <slot>calibrationDataWriter2Srv/update</slot>
        </connect>

        <connect>
            <signal>loadInputsAct/clicked</signal>
            <slot>calibrationDataReader1Srv/openLocationDialog</slot>
            <slot>calibrationDataReader1Srv/update</slot>
            <slot>calibrationDataReader2Srv/openLocationDialog</slot>
            <slot>calibrationDataReader2Srv/update</slot>
        </connect>

        <!-- Camera/grabbers connections -->
        <connect>
            <signal>videoGrabber1Srv/cameraStarted</signal>
            <signal>videoGrabber2Srv/cameraStarted</signal>
            <slot>pauseVideoAct/show</slot>
            <slot>startVideoAct/hide</slot>
            <slot>stopVideoAct/enable</slot>
            <slot>loopVideoAct/enable</slot>
        </connect>

        <connect>
            <signal>${camera}/idModified</signal>
            <slot>videoGrabber1Srv/stopCamera</slot>
            <slot>videoGrabber2Srv/stopCamera</slot>
        </connect>

        <connect>
            <signal>${camera}/modified</signal>
            <slot>stopVideoAct/update</slot>
        </connect>

        <!-- Manage the video slider -->
        <connect>
            <signal>videoSliderSrv/positionChanged</signal>
            <slot>videoGrabber1Srv/setPositionVideo</slot>
            <slot>videoGrabber2Srv/setPositionVideo</slot>
        </connect>

        <connect>
            <signal>videoGrabber1Srv/positionModified</signal>
            <signal>videoGrabber2Srv/positionModified</signal>
            <slot>videoSliderSrv/setPositionSlider</slot>
        </connect>

        <connect>
            <signal>videoGrabber1Srv/durationModified</signal>
            <signal>videoGrabber2Srv/durationModified</signal>
            <slot>videoSliderSrv/setDurationSlider</slot>
        </connect>

        <connect>
            <signal>chessboardDetectorSrv/chessboardDetected</signal>
            <slot>frameSynchronizerSrv/synchronize</slot>
            <slot>detectionStatusSrv/toggleGreenRed</slot>
        </connect>

        <connect>
            <signal>chessboardDetectorSrv/updated</signal>
            <slot>genericSceneSrv/requestRender</slot>
            <slot>frameSynchronizerSrv/synchronize</slot>
        </connect>

        <connect>
            <signal>${calibrationInfo1}/getRecord</signal>
            <slot>displayCalibrationInfoSrv/displayImage</slot>
        </connect>

        <connect>
            <signal>${calibrationInfo2}/addedRecord</signal>
            <signal>${calibrationInfo2}/removedRecord</signal>
            <signal>${calibrationInfo2}/resetRecord</signal>
            <signal>calibrationDataReader2Srv/updated</signal>
            <slot>calibrationInfoEditorSrv/update</slot>
        </connect>

        <connect>
            <signal>openCVExtrinsicCalSrv/errorComputed</signal>
            <slot>errorLabelSrv/setDoubleParameter</slot>
        </connect>

        <connect channel="${preferencesModifiedProxy}">
            <slot>openCVExtrinsicCalSrv/updateChessboardSize</slot>
            <slot>chessboardDetectorSrv/updateChessboardSize</slot>
            <slot>calibrationDataReader1Srv/updateChessboardSize</slot>
            <slot>calibrationDataReader2Srv/updateChessboardSize</slot>
        </connect>

        <connect>
            <signal>mainView/started</signal>
            <slot>videoSliderView/show</slot>
        </connect>

        <!-- ******************************* Start/Stop services ***************************************** -->

        <start uid="mainView" />
        <start uid="openCVExtrinsicCalSrv" />
        <start uid="chessboardDetectorSrv" />
        <start uid="displayCalibrationInfoSrv" />
        <start uid="extrinsicExporterSrv" />
        <start uid="frameSynchronizerSrv" />
        <start uid="videoGrabber1Srv" />
        <start uid="videoGrabber2Srv" />
        <start uid="calibrationDataWriter1Srv" />
        <start uid="calibrationDataWriter2Srv" />
        <start uid="calibrationDataReader1Srv" />
        <start uid="calibrationDataReader2Srv" />
        <start uid="video1Adp" />
        <start uid="video2Adp" />

    </config>
</extension>
